//
//  SceneDelegate.h
//  LX Logistics Pro
//
//  Created by Jeff Kase on 8/27/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

