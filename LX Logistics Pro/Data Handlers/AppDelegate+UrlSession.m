//
//  AppDelegate+NSURLSession.m
//  Locator
//
//  Created by Jeff Kase on 4/4/16.
//  Copyright © 2016 LocatorX. All rights reserved.
//

#import "AppDelegate+UrlSession.h"
#import "Lockbox.h"

@implementation AppDelegate (UrlSession)

-(void)initializeSession
{
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLog(@"current language: %@", language);
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfiguration setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
    
    self.urlSession = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:nil delegateQueue:nil];
    
    [self appSettings:kAppName withBlock:nil];
}

-(BOOL)hasCredentials
{
    NSString *email = [Lockbox stringForKey:@"email"];
    NSString *password = [Lockbox stringForKey:@"password"];
    return (email.length > 0 && password.length > 0);
}

-(void)appSettings:(NSString *)appName withBlock:(void(^)(NSMutableDictionary *json)) block
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@appSettings/mobile/%@", kAPIURLBase, appName]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];
    
    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];

            self.privacyPolicy = json[@"privacyPolicy"];
            if (!self.privacyPolicy || self.privacyPolicy.length == 0)
                self.privacyPolicy = kPrivacyPolicy;
            
            self.termsOfUse = json[@"termsOfUse"];
            if (!self.termsOfUse || self.termsOfUse.length == 0)
                self.termsOfUse = kTermsOfUse;

            /*
            self.connectHome = json[@"connectHome"];
            if (!self.connectHome || self.connectHome.length == 0)
                self.connectHome = kScanPageUrl;
             */

//            [[NSNotificationCenter defaultCenter] postNotificationName:kAppSettingsChange object:nil];
            
            if (block)
                block(json);
        } else {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        }
    }] resume];
}

-(void)checkAuthToken:(void(^)(NSMutableDictionary *json)) block
{
    NSLog(@"***** checkAuthToken *****");
    
    if (self.token) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@auth/mobile", kAPIURLBase]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];
        
        [request setHTTPMethod:@"POST"];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:@{@"userSessionId":self.token} options:0 error:&error];
        [request setHTTPBody:postData];
        
        [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (data) {
                NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                if (json[@"user"]) {
                    NSLog(@"***** successful auth *****");
                    [self assignLoginElementsFromJson:json];
                    
                    if (block)
                        block(json);
                } else {
                    [self loginWithBlock:block];
                }
            } else {
                [self loginWithBlock:block];
            }
        }] resume];
    } else {
        [self loginWithBlock:nil];
    }
}

-(void)assignLoginElementsFromJson:(NSMutableDictionary *)json
{
    self.user = json[@"user"];
    self.organization = json[@"organization"];
    self.forms = json[@"forms"];
    self.actions = json[@"actions"];
    
    self.token = json[@"token"];
    self.privacyPolicy = json[@"privacyPolicy"];
    if (!self.privacyPolicy || self.privacyPolicy.length == 0)
        self.privacyPolicy = kPrivacyPolicy;
    self.termsOfUse = json[@"termsOfUse"];
}

-(void)loginWithBlock:(void(^)(NSMutableDictionary *json)) block
{
    NSString *email = [Lockbox stringForKey:@"email"];
    NSString *password = [Lockbox stringForKey:@"password"];
    [self login:email withPassword:password andBlock:block];
#ifdef NotNow
    NSMutableDictionary *json = [[self JSONFromFile] mutableCopy];
    
    self.user = json[@"user"];
    self.organization = json[@"organization"];
    self.forms = json[@"forms"];
    self.actions = json[@"actions"];
    
    self.token = json[@"token"];
    self.privacyPolicy = json[@"privacyPolicy"];
    if (!self.privacyPolicy || self.privacyPolicy.length == 0)
        self.privacyPolicy = kPrivacyPolicy;
    self.termsOfUse = json[@"termsOfUse"];
//                [self setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
    if (block)
        block(json);
#endif
}

- (NSDictionary *)JSONFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"login" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

-(void)login:(NSString *)email withPassword:(NSString *)password andBlock:(void(^)(NSMutableDictionary *json)) block
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@auth/mobile", kAPIURLBase]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];
    
    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:@{@"email":email,@"password":password} options:0 error:&error];
    [request setHTTPBody:postData];
    
    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (json[@"user"]) {
                [Lockbox setString:email forKey:@"email"];
                [Lockbox setString:password forKey:@"password"];
                
                NSLog(@"login: %@", json);

                [self assignLoginElementsFromJson:json];
                
//                [self setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
            }
            if (block)
                block(json);
        } else {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        }
    }] resume];
}

-(void)forgotPasswordRequest:(NSString *)email withBlock:(void(^)(NSMutableDictionary *json)) block
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@appUsers/forgotPasswordRequest", kAPIURLBase]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];
    
    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:@{
        @"email":email,
        @"sourceSuffix":@"LogisticsPro"
    } options:0 error:&error];
    [request setHTTPBody:postData];
    
    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//            if (json[@"user"]) {

//            }
            if (block)
                block(json);
        } else {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        }
    }] resume];
}

-(void)setRootViewController:(UIViewController *)viewController
{
    if ([NSThread isMainThread]) {
        self.window.rootViewController = viewController;
    } else {
        [self performSelectorOnMainThread:@selector(setRootViewController:) withObject:viewController waitUntilDone:NO];
    }
}

-(void)logout
{
    [Lockbox setString:nil forKey:@"email"];
    [Lockbox setString:nil forKey:@"password"];
    
    [self setRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController]];
}

-(void)postAssetScan:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block
{
    NSURL *url = nil;
    if (parameters[@"assetId"])
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@assets/%@/scanFromApp", kAPIURLBase, parameters[@"assetId"]]];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSLog(@"getAssetScan: %@", url.absoluteString);
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:self.token forHTTPHeaderField:@"auth-token"];
//    [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];

    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    [request setHTTPBody:postData];

    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"getAssetScan: %@", json);
            block(json);
        } else {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        }
    }] resume];
}

-(void)postExternalAssetScan:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block
{
    NSURL *url = nil;
    if (parameters[@"assetId"])
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@assets/externalScanFromApp", kAPIURLBase]];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSLog(@"getAssetScan: %@", url.absoluteString);
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:self.token forHTTPHeaderField:@"auth-token"];
//    [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];

    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    [request setHTTPBody:postData];

    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"getAssetScan: %@", json);
            block(json);
        } else {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        }
    }] resume];
}

-(void)postAssetAction:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@assets/%@/action", kAPIURLBase, parameters[@"assetId"]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:self.token forHTTPHeaderField:@"auth-token"];

    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    [request setHTTPBody:postData];
    
    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (block)
                block(json);
        } else {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        }
    }] resume];
}

@end
