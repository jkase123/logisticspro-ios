//
//  AppDelegate+NSURLSession.h
//  Locator
//
//  Created by Jeff Kase on 4/4/16.
//  Copyright © 2016 LocatorX. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (UrlSession)

@property (readonly) BOOL hasCredentials;

-(void)initializeSession;

-(void)setRootViewController:(UIViewController *)viewController;

-(void)appSettings:(NSString *)appName withBlock:(void(^)(NSMutableDictionary *json)) block;

-(void)checkAuthToken:(void(^)(NSMutableDictionary *json)) block;
-(void)login:(NSString *)email withPassword:(NSString *)password andBlock:(void(^)(NSMutableDictionary *json)) block;
-(void)loginWithBlock:(void(^)(NSMutableDictionary *json)) block;
-(void)logout;

-(void)forgotPasswordRequest:(NSString *)email withBlock:(void(^)(NSMutableDictionary *json)) block;

-(void)postAssetScan:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block;
-(void)postExternalAssetScan:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block;
-(void)postAssetAction:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block;

@end
