//
//  AppDelegate+Location.m
//  Locator
//
//  Created by Jeff Kase on 4/5/16.
//  Copyright © 2016 LocatorX. All rights reserved.
//

#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
//#import "AppDelegate+Data.h"
//#import "NSMutableDictionary+AppUser.h"

@implementation AppDelegate (Location)

-(void)initializeLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationActive = NO;
    self.locationServicesAuthorized = NO;
    self.lastLocationReportTime = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLocationChanged:) name:kUpdatedLocation object:nil];
    
    [self checkAuthorization];
}

-(void)userLocationChanged:(NSNotification *)notification
{
//    NSLog(@"**** location now %f,%f", self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude);
#ifdef NotNow
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval since = now - self.lastLocationReportTime;
    
    bool sendReport = NO;
    if (self.locationActive) {
        sendReport = (since > 1.0);
    } else {
        sendReport = (since > 600.0);
    }
    
    if (sendReport) {
        self.lastLocationReportTime = now;
        int userLocatorId = [self.appUser userLocatorIdForLocatorId:1];
        
        NSMutableDictionary *json = [@{
                                       @"userLocatorId":@(userLocatorId),
                                       @"latitude":@(self.currentLocation.coordinate.latitude),
                                       @"longitude":@(self.currentLocation.coordinate.longitude)
                                       } mutableCopy];
    }
#endif
}

-(void)setActiveLocation:(BOOL)isActive
{
    // in foreground and high accuracy (during event)
    if (isActive) {
        self.locationActive = YES;
        self.locationManager.pausesLocationUpdatesAutomatically = NO;
        self.locationManager.activityType = kCLLocationAccuracyBest;
        [self.locationManager disallowDeferredLocationUpdates];
    } else {
        self.locationActive = NO;
        self.locationManager.pausesLocationUpdatesAutomatically = YES;
        self.locationManager.activityType = kCLLocationAccuracyNearestTenMeters;
//        [self.locationManager allowDeferredLocationUpdatesUntilTraveled:20 timeout:600];    // 10 minutes or 20 meters to trigger update
    }
}

-(void)checkAuthorization
{
    CLAuthorizationStatus authStatus = [CLLocationManager authorizationStatus];
    
    if (authStatus == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestLocation];
    } else {
        [self.locationManager requestLocation];
    }
}

-(void)getLocationWithBlock:(void(^)(CLLocation *location)) block
{
    self.locationBlock = block;
    [self.locationManager requestLocation];
}

-(void)startUpdatingLocationWithBlock:(void(^)(CLLocation *location)) block
{
    self.locationBlock = block;
    [self.locationManager startUpdatingLocation];
}

-(void)stopUpdatingLocation
{
    self.locationBlock = nil;
    self.locationManager.pausesLocationUpdatesAutomatically = YES;
    self.locationManager.activityType = kCLLocationAccuracyNearestTenMeters;
    [self.locationManager allowDeferredLocationUpdatesUntilTraveled:20 timeout:600];    // 10 minutes or 20 meters to trigger update
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    self.currentLocation = [locations lastObject];
    if (self.locationBlock)
        self.locationBlock(self.currentLocation);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatedLocation object:nil userInfo:nil];

    /*
    if (self.inBackground || !self.locationActive)
        [self.locationManager allowDeferredLocationUpdatesUntilTraveled:20 timeout:600];    // 10 minutes or 20 meters to trigger update
     */
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"location failed: %@", error.localizedDescription);
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    self.locationServicesAuthorized = (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse);
    
    [self.locationManager requestLocation];
    [self.locationManager startUpdatingLocation];
    [self setActiveLocation:NO];
}

@end
