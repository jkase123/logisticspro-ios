//
//  ViewController.m
//  ProductScan
//
//  Created by Jeff Kase on 6/8/19.
//  Copyright © 2019 Jeff Kase. All rights reserved.
//

#ifdef ENABLE_NFC
@import CoreNFC;
@interface ViewController () <NFCNDEFReaderSessionDelegate, QRScannerResult, WKNavigationDelegate, WKUIDelegate, UIPopoverPresentationControllerDelegate, SimpleListHandler>
@property (nonatomic) NFCNDEFReaderSession *nfcSession;
#endif
@import WebKit;
@import AudioToolbox;
@import AVFoundation;

#import "ViewController.h"
#import "QRScannerViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "NSCombined+Values.h"
#import "SimpleListViewController.h"
#import "UIColor+Convert.h"
#import "AlertViewController.h"
#import "AppDelegate+UrlSession.h"
#import "UIViewController+Alert.h"
#import "Lockbox.h"

@interface ViewController () <QRScannerResult, WKNavigationDelegate, WKUIDelegate, UIPopoverPresentationControllerDelegate, SimpleListHandler>

@property (weak, nonatomic) IBOutlet WKWebView *webView;

@property (nonatomic) NSString *qrResult;
@property (nonatomic) NSString *nfcResult, *assetId, *url, *scanMode;
@property (nonatomic) NSArray *affinities, *scanModes;

@property SystemSoundID beepSound;

@end

#ifdef STAGING
#define kHomePageUrl @"https://lxconnect-staging.locatorx.com/token/"
#define kAssetPageUrl @"https://lxconnect-staging.locatorx.com/asset/"
#else
    #ifdef DEV
        #define kHomePageUrl @"https://lxconnect-dev.locatorx.com/token/"
        #define kAssetPageUrl @"https://lxconnect-dev.locatorx.com/asset/"
    #else
        #define kHomePageUrl @"https://lxconnect.locatorx.com/token/"
        #define kAssetPageUrl @"https://lxconnect.locatorx.com/asset/"
    #endif
#endif

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;

    self.scanModes = @[
                        @"Both",
                        @"QR Code Only",
                        @"NFC Only"
                        ];
    self.scanMode = @"QR Code Only";
    
    NSSet *websiteDataTypes = [NSSet setWithArray:@[
                                                    WKWebsiteDataTypeDiskCache,
                                                    WKWebsiteDataTypeOfflineWebApplicationCache,
                                                    WKWebsiteDataTypeMemoryCache,
                                                    //WKWebsiteDataTypeLocalStorage,
                                                    WKWebsiteDataTypeCookies,
                                                    //WKWebsiteDataTypeSessionStorage,
                                                    //WKWebsiteDataTypeIndexedDBDatabases,
                                                    //WKWebsiteDataTypeWebSQLDatabases,
                                                    WKWebsiteDataTypeFetchCache, //(iOS 11.3, *)
                                                    //WKWebsiteDataTypeServiceWorkerRegistrations, //(iOS 11.3, *)
                                                    ]];
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
    }];
    
    self.webView.scrollView.scrollEnabled = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSString * url = [NSString stringWithFormat:@"%@%@/appUserId/%@", kHomePageUrl, self.appDelegate.token, self.appDelegate.user[@"appUserId"]];
    NSLog(@"url: %@", url);
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];

    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [session setActive:YES error:nil];
    
}

- (IBAction)scanPressed:(id)sender
{
    NSLog(@"start scanning");
    if ([self.scanMode isEqualToString:@"NFC Only"]) {
        [self startNFCScan];
    } else {
        [self performSegueWithIdentifier:@"QRScan" sender:self];
//        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"QRScanner"] animated:YES completion:nil];
    }
}

- (IBAction)backPressed:(id)sender
{
    [self.webView goBack];
}

- (IBAction)forwardPressed:(id)sender
{
    [self.webView goForward];
}

-(void)found:(NSString *)result
{
    NSLog(@"found result: %@", result);
    self.qrResult = result;
    
    if ([self.scanMode isEqualToString:@"Both"])
        [self startNFCScan];
    else
        [self processAssetScan];
}

-(void)startNFCScan
{
#ifdef ENABLE_NFC
    if (!self.nfcSession) {
        self.nfcResult = nil;
        
        self.nfcSession = [[NFCNDEFReaderSession alloc] initWithDelegate:self queue:nil invalidateAfterFirstRead:NO];
        [self.nfcSession beginSession];
    }
#endif
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *result))completionHandler
{
    NSString *result = @"done";
    if ([prompt hasPrefix:@"scan"]) {
        [self scanPressed:self];
    } else if ([prompt hasPrefix:@"logout"]) {
        [self.appDelegate logout];
    } else if ([prompt hasPrefix:@"settings"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
    } else if ([prompt hasPrefix:@"scrollEnable"]) {
        self.webView.scrollView.scrollEnabled = YES;
    } else if ([prompt hasPrefix:@"scrollDisable"]) {
        self.webView.scrollView.scrollEnabled = NO;
    } else if ([prompt hasPrefix:@"tokenExpired"]) {
        [self alertWithTitle:@"Error"
                  andMessage:@"Your session has expired"
                cancelButton:@"OK"
                  andButtons:nil
               andTextFields:nil
                    andBlock:^(int index, NSArray *textFields) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.appDelegate logout];
                                });
        }];
    } else if ([prompt hasPrefix:@"get="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"set="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 3) {
            result = [Lockbox stringForKey:items[2]];
            [Lockbox setString:items[2] forKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"toggle="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
            if (!result || [result isEqualToString:@"false"]) {
                result = @"true";
                [Lockbox setString:@"true" forKey:items[1]];
            } else {
                result = @"false";
                [Lockbox setString:@"false" forKey:items[1]];
            }
        }
    }
    completionHandler(result);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    self.backButton.enabled = webView.canGoBack;
//    self.forwardButton.enabled = webView.canGoForward;
}

#ifdef ENABLE_NFC
- (void)readerSession:(nonnull NFCNDEFReaderSession *)session didDetectNDEFs:(nonnull NSArray<NFCNDEFMessage *> *)messages
{
    NSLog(@"didDetect");
    NSLog(@"  %i messages", (int)messages.count);
    for (NFCNDEFMessage *message in messages) {
        for (NFCNDEFPayload *record in message.records) {
            unsigned char buffer[2000];
            memset(buffer, 0, 2000);

            [record.payload getBytes:buffer length:200];
            if (record.typeNameFormat == NFCTypeNameFormatNFCWellKnown) {
                self.nfcResult = [NSString stringWithCString:(const char *)(buffer + 3) encoding:NSUTF8StringEncoding];
//                [self processAssetScan];
            }
        }
    }
    [self.nfcSession invalidateSession];
    self.nfcSession = nil;
}

- (void)readerSession:(nonnull NFCNDEFReaderSession *)session didInvalidateWithError:(nonnull NSError *)error
{
    NSLog(@"didInvalidateWithError: %@", error.localizedDescription);
    self.nfcSession = nil;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self processAssetScan];
    });
}
#endif

-(void)processAssetScan
{
    NSArray<NSString *> *parts = [self.qrResult componentsSeparatedByString:@"/"];
    for (NSString *part in parts) {
        //        NSLog(@"part: %@", part);
        if (part.length == 36) {
            self.assetId = part;
            break;
        }
    }

    [self fetchAssetScanInfo];
}

-(void)fetchAssetScanInfo
{
    NSString *sound = [Lockbox stringForKey:@"sound"];
    if ([@"true" isEqualToString:sound]) {
        SystemSoundID soundId;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Chime" ofType:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundId);
        AudioServicesPlaySystemSoundWithCompletion(soundId, ^{
            NSLog(@"play sound");
            AudioServicesDisposeSystemSoundID(soundId);
        });
    }
    
    NSString *vibrate = [Lockbox stringForKey:@"vibrate"];
    if ([@"true" isEqualToString:vibrate])
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

    if (self.assetId) {
        self.url = [NSString stringWithFormat:@"%@%@/lat/%0.3f/lon/%0.3f", kAssetPageUrl, self.assetId, self.appDelegate.currentLocation.coordinate.latitude, self.appDelegate.currentLocation.coordinate.longitude];
        NSLog(@"opening %@", self.url);
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
        return;
    }

    NSLog(@"fetchAssetScanInfo %@ %@", self.assetId, (self.nfcResult ? self.nfcResult : @"null"));
    NSMutableDictionary *postBody = [@{
                               @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
                               @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
                               } mutableCopy];
    if (self.assetId)
        postBody[@"assetId"] = self.assetId;
    if (self.nfcResult)
        postBody[@"nfcString"] = self.nfcResult;

    [self.appDelegate postAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
        NSLog(@"**** asset scan result");
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSMutableDictionary *assetMap = json[@"asset"];

            [self showSecurityElements:[json[@"isCounterfeit"] safeBoolValue]
                             missingQR:[json[@"missingQR"] safeBoolValue]
                            missingNFC:[json[@"missingNFC"] safeBoolValue]
                              nfcValid:[json[@"nfcValid"] safeBoolValue]];
            if (!self.assetId && assetMap != nil)
                self.assetId = assetMap[@"assetId"];
            if (self.assetId) {
                self.url = [NSString stringWithFormat:@"%@%@/lat/%0.3f/lon/%0.3f", kAssetPageUrl, self.assetId, self.appDelegate.currentLocation.coordinate.latitude, self.appDelegate.currentLocation.coordinate.longitude];
            }
            NSLog(@"Opening: %@", self.url);
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
        });
    }];
}

-(void)showSecurityElements:(BOOL)isCounterfeit missingQR:(BOOL)missingQR missingNFC:(BOOL)missingNFC nfcValid:(BOOL)nfcValid
{
    /*
    BOOL showAlert = NO;
    UIColor *alertBackgroundColor;
    NSString *alertText;
    
    if (isCounterfeit) {
        showAlert = YES;
        alertBackgroundColor = [UIColor colorFromHexString:@"ca670f"];
        alertText = @"This product may be counterfeit";
        [self.lockImage setImage:[UIImage imageNamed:@"orange_lock"]];
    }
    if (nfcValid && !isCounterfeit) {
        if (missingQR && ![self.scanMode isEqualToString:@"NFC Only"]) {
            showAlert = YES;
            alertBackgroundColor = [UIColor colorFromHexString:@"2348d2"];
            alertText = @"No QR Code was found";
            [self.lockImage setImage:[UIImage imageNamed:@"blue_lock"]];
        } else
            [self.lockImage setImage:[UIImage imageNamed:@"green_lock"]];
    }
    if (!missingNFC && !nfcValid) {
        showAlert = YES;
        alertBackgroundColor = [UIColor colorFromHexString:@"ca0f0f"];
        alertText = @"This product is likely counterfeit";
        [self.lockImage setImage:[UIImage imageNamed:@"red_lock"]];
    }
    if (!isCounterfeit && missingNFC) {
        showAlert = YES;
        alertBackgroundColor = [UIColor colorFromHexString:@"2348d2"];
        alertText = @"No NFC chip was found";
        [self.lockImage setImage:[UIImage imageNamed:@"blue_lock"]];
    }
    if (showAlert) {
        AlertViewController *avc = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertPopup"];
        avc.modalPresentationStyle = UIModalPresentationPopover;
        avc.preferredContentSize = CGSizeMake(300, 80);
        avc.view.backgroundColor = alertBackgroundColor;
        avc.popoverPresentationController.delegate = self;
        avc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        avc.popoverPresentationController.sourceView = self.lockImage;
        avc.popoverPresentationController.sourceRect = self.lockImage.bounds;
        avc.label.text = alertText;
        
        [self presentViewController:avc animated:YES completion:nil];
        
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    }
     */
}

#pragma mark - SimpleList

-(void)simpleListDidSelect:(NSString *)name atIndex:(NSInteger)index andSection:(NSInteger)section
{
    NSLog(@"popover result: %@ %li", name, (long)section);
#ifdef NotNow
    switch (section) {
        case 0:
        {
            self.affinity = name;
            if (self.qrResult) {
                self.url = [self.qrResult stringByReplacingOccurrencesOfString:@"Consumer" withString:self.affinity];
                [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
            }
        }
            break;
        case 1:
            self.scanMode = name;
            break;
    }
#else
    self.scanMode = name;
#endif
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"QRScan" isEqualToString:segue.identifier]) {
        QRScannerViewController *qrvc = segue.destinationViewController;
        qrvc.handler = self;
    }
#ifdef NotNow
    else if ([@"AffinityPopover" isEqualToString:segue.identifier]) {
        SimpleListViewController *affinityPopover = segue.destinationViewController;
        int iSize = (int)(self.scanModes.count + 2) * [SimpleListViewController rowHeight];
//        int iSize = (int)(self.affinities.count + self.scanModes.count + 2) * [SimpleListViewController rowHeight];
        if (iSize > 400)
            iSize = 400;
        affinityPopover.preferredContentSize = CGSizeMake(200, iSize);
//        affinityPopover.affinities = self.affinities;
        affinityPopover.scanModes = self.scanModes;
//        affinityPopover.affinity = self.affinity;
        affinityPopover.scanMode = self.scanMode;
        affinityPopover.handler = self;
        
        // configure the Popover presentation controller
        UIPopoverPresentationController *popoverController = [affinityPopover popoverPresentationController];
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        popoverController.sourceView = self.settingsButton;
        popoverController.sourceRect = self.settingsButton.bounds;
        popoverController.delegate = self;
    }
#endif
}

@end
