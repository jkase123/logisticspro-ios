//
//  HistoryDetailViewController.m
//  LX Connect
//
//  Created by Jeff Kase on 7/15/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "HistoryDetailViewController.h"
#import "NSMutableDictionary+Event.h"

@interface HistoryDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *assetTag;
@property (weak, nonatomic) IBOutlet UILabel *facility;
@property (weak, nonatomic) IBOutlet UILabel *action;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *assetType;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UILabel *latLon;
@property (weak, nonatomic) IBOutlet UILabel *note;

@end

@implementation HistoryDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.assetTag.text = self.event.assetTag;
    self.facility.text = self.event.facility;
    self.action.text = self.event.action;
    self.time.text = self.event.timeOfLogString;
    self.assetType.text = self.event.assetType;
    self.location.text = self.event.location;
//    self.location.text = [NSString stringWithFormat:@"%@, %@ %@", self.event.city, self.event.state, self.event.country];
    self.latLon.text = [NSString stringWithFormat:@"%0.4f,%0.4f", self.event.latitude, self.event.longitude];
    self.note.text = self.event.note;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (IBAction)backPushed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)editAssetPressed:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

@end
