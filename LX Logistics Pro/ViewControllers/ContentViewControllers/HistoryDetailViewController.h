//
//  HistoryDetailViewController.h
//  LX Connect
//
//  Created by Jeff Kase on 7/15/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface HistoryDetailViewController : UIViewController

@property (nonatomic) NSMutableDictionary *event;
//@property (weak) HomeViewController *homeViewController;

@end

NS_ASSUME_NONNULL_END
