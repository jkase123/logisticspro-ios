//
//  ScanViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import AudioToolbox;
@import AVFoundation;

#import "ScanViewController.h"
#import "QRScannerViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "NSCombined+Values.h"
#import "UIColor+Convert.h"
#import "UIViewController+Alert.h"
#import "AlertViewController.h"
#import "Lockbox.h"
#import "FormViewController.h"

@interface ScanViewController () <QRScannerResult>

@property (nonatomic) NSString *url;
@property (nonatomic) NSMutableDictionary *assetMap;
@property (nonatomic) NSArray *catalogues;
@property (weak) UIViewController *scannerViewController;
@property (weak, nonatomic) IBOutlet UINavigationItem *profileButton;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@end

@implementation ScanViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *result = [Lockbox stringForKey:@"sound"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"sound"];
    
    result = [Lockbox stringForKey:@"vibrate"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"vibrate"];

    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    if (!self.navigationItem.rightBarButtonItem.image)
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
    //    if (@available(iOS 13, *)) {
    //        NSLog(@"got 13");
    //    } else {
    //        NSLog(@"prior to 13");
    //        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
    //    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"LocatorX"]];

    self.scanButton.backgroundColor = self.appDelegate.buttonColor;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scannerClosed:) name:kScannerClosed object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)scanPressed
{
    [self performSegueWithIdentifier:@"QRScan" sender:self];
}

-(void)found:(NSString *)result withSourceTag:(NSInteger)tag viewController:(UIViewController *)viewController scanType:(NSString *)scanType objectType:(NSString*)objectType andBlock:(void(^)(NSArray *messages, bool done)) block
{
    NSLog(@"found result: %@ %@", result, objectType);
    
    self.scannerViewController = viewController;

    if ([objectType isEqualToString:AVMetadataObjectTypeQRCode]) {
        if (![self setAssetIdFromResult:result]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self alertWithTitle:@"Error"
                          andMessage:@"Invalid QR Code"
                        cancelButton:@"OK"
                          andButtons:nil
                       andTextFields:nil
                            andBlock:^(int iCount, NSArray *fields){
                    block(nil, false);
                }
                 ];
            });
        } else
            [self fetchAssetScanInfoWithBlock:block];
    } else {
        [self processExternalScanInfo:result andBlock:block];
    }

}

-(void)processExternalScanInfo:(NSString *)result andBlock:(void(^)(NSArray *messages, bool done)) block
{
    NSMutableDictionary *postBody = [@{
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
    } mutableCopy];

    postBody[@"assetId"] = result;
    
    [self.appDelegate postExternalAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
        if (json && json[@"asset"]) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.assetMap = json[@"asset"];
                self.catalogues = json[@"catalogues"];
                if (self.assetMap) {
                    [self.navigationController popViewControllerAnimated:NO];
                    [self performSegueWithIdentifier:@"Form" sender:self];
                } else {
                    [self alertWithTitle:@"Error"
                              andMessage:@"Invalid Scan"
                            cancelButton:@"OK"
                              andButtons:nil
                           andTextFields:nil
                                andBlock:^(int iCount, NSArray *fields){
                                    block(nil, false);
                                }
                    ];
                }
            });
        }
    }];
}

-(void)fetchAssetScanInfoWithBlock:(void(^)(NSArray *messages, bool done)) block
{
    NSString *sound = [Lockbox stringForKey:@"sound"];
    if ([@"true" isEqualToString:sound]) {
        SystemSoundID soundId;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Chime" ofType:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundId);
        AudioServicesPlaySystemSoundWithCompletion(soundId, ^{
            NSLog(@"play sound");
            AudioServicesDisposeSystemSoundID(soundId);
        });
    }
    
    NSString *vibrate = [Lockbox stringForKey:@"vibrate"];
    if ([@"true" isEqualToString:vibrate])
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

    NSLog(@"fetchAssetScanInfo %@ %@", self.assetId, @"null");
    
    NSMutableDictionary *postBody = [@{
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
    } mutableCopy];
    if (self.assetId)
        postBody[@"assetId"] = self.assetId;
    if (self.qrHash)
        postBody[@"qrHash"] = self.qrHash;
    
    [self.appDelegate postAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
        NSLog(@"**** asset scan result");
        dispatch_sync(dispatch_get_main_queue(), ^{
            self.assetMap = json[@"asset"];
            self.catalogues = json[@"catalogues"];
            if (self.assetMap) {
                [self.navigationController popViewControllerAnimated:NO];
                [self performSegueWithIdentifier:@"Form" sender:self];
            } else {
                [self alertWithTitle:@"Error"
                          andMessage:@"Invalid QR Code"
                        cancelButton:@"OK"
                          andButtons:nil
                       andTextFields:nil
                            andBlock:^(int iCount, NSArray *fields){
                                block(nil, false);
                            }
                ];
            }
        });
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"QRScan" isEqualToString:segue.identifier]) {
        QRScannerViewController *qrvc = segue.destinationViewController;
        qrvc.handler = self;
        qrvc.sourceTag = -1;
        qrvc.scanType = @"scan";
    } else if ([@"Form" isEqualToString:segue.identifier]) {
        FormViewController *fvc = segue.destinationViewController;
        fvc.assetMap = self.assetMap;
        fvc.catalogues = self.catalogues;
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

@end
