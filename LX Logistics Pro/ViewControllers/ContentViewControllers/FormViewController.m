//
//  FormViewController.m
//  LX Connect
//
//  Created by Jeff Kase on 4/9/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//
@import WebKit;
@import AudioToolbox;
@import AVFoundation;

#import "FormViewController.h"
#import "QRScannerViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Data.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "NSCombined+Values.h"
#import "SimpleListViewController.h"
#import "UIColor+Convert.h"
#import "UIViewController+Alert.h"
#import "Lockbox.h"
#import "NSMutableArray+Events.h"
#import "NSMutableDictionary+Event.h"
#import "SimpleListViewController.h"
#import "TableFooterView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface FormTableTextEntryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *textEntry;

@end

@implementation FormTableTextEntryCell

-(id)initWithCoder:(NSCoder *)coder
{
    if ((self = [super initWithCoder:coder])) {
        self.textEntry.layer.backgroundColor = [[UIColor redColor] CGColor];
        self.textEntry.layer.cornerRadius=10;
        self.textEntry.layer.masksToBounds=YES;
        self.textEntry.layer.borderColor=[[UIColor redColor] CGColor];
        self.textEntry.layer.borderWidth= 5;
    }
    return self;
}

@end

@interface FormTableAssetsEntryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UILabel *numAdded;
@property (weak) FormViewController *fvc;

@end

@implementation FormTableAssetsEntryCell

- (IBAction)scanChildPressed:(id)sender
{
    [self.fvc performSegueWithIdentifier:@"QRScan" sender:self];
}

@end

@interface FormTableExternalEntryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UILabel *numAdded;
@property (weak) FormViewController *fvc;

@end

@implementation FormTableExternalEntryCell

- (IBAction)scanChildPressed:(id)sender
{
    [self.fvc performSegueWithIdentifier:@"ExternalScan" sender:self];
}

@end

@interface FormTableLocationEntryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UILabel *binLocation;
@property (weak) FormViewController *fvc;

@end

@implementation FormTableLocationEntryCell

- (IBAction)scanChildPressed:(id)sender
{
    [self.fvc performSegueWithIdentifier:@"LocationScan" sender:self];
}

@end

@interface FormTableAssetCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation FormTableAssetCell

@end

@interface FormTableSelectEntryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) UIViewController *uvc;

@end

@implementation FormTableSelectEntryCell

@end

@interface FormSectionHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation FormSectionHeaderCell

@end

@interface FormFooterView : UIView

@end

@implementation FormFooterView

@end

@interface FormViewController () <QRScannerResult, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, SimpleListHandler, WKNavigationDelegate, WKUIDelegate>

@property (nonatomic) NSString *qrResult;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *formScrollView;
@property (weak, nonatomic) IBOutlet WKWebView *webView;

@property (nonatomic) NSMutableArray *actionDictArray, *actionsArray, *actionTagsArray, *catalogueNameArray, *formOptionValues, *formDictArray;
@property (nonatomic) NSMutableArray *formFieldArray, *scannedAssets, *scannedAssetIds;
@property (nonatomic) NSString *location;
@property NSInteger selectedActionIndex;
@property (weak) NSMutableDictionary *selectedFormDict;
@property (nonatomic) NSString *lastChildAssetTag;
//@property (weak, nonatomic) NSMutableDictionary *currentFormField;
@property (weak, nonatomic) UIButton *currentFormSelectButton;

@end

#define kLocationScanMessage @"Scan a Location"

@implementation FormViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.formFieldArray = [NSMutableArray array];
    
//    self.tableView.backgroundColor = [UIColor colorFromHexString:@"555A9C"];
    TableFooterView *tfv = [[TableFooterView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
    [tfv setHasData:YES alternateMessage:@""];
    self.tableView.tableFooterView = tfv;

    self.assetId = self.assetMap[@"assetId"];
    
    if (!self.actionsArray) {
        NSLog(@"Form:viewDidLoad actionsArray is nil");
        
        self.actionsArray = [NSMutableArray arrayWithCapacity:self.appDelegate.actions.count];
        self.actionTagsArray = [NSMutableArray arrayWithCapacity:self.appDelegate.actions.count];
        self.actionDictArray = [NSMutableArray arrayWithCapacity:self.appDelegate.actions.count];
        for (NSString *actionTag in self.appDelegate.actions) {
            NSDictionary *action = self.appDelegate.actions[actionTag];
            if (action[@"label"])
                [self.actionsArray addObject:action[@"label"]];
            else
                [self.actionsArray addObject:actionTag];
            [self.actionTagsArray addObject:actionTag];
            [self.actionDictArray addObject:action];
        }
        
//        self.documentDictArray = [@[@{@"url" : @"https://wiki.locatorx.com/repairManuals.html", @"title" : @"Repair Manuals"}, @{@"url" : @"https://wiki.locatorx.com/technicalDocs.html", @"title" : @"Technical Documents"}] mutableCopy];
//        self.catalogueNameArray = [@[@"Repair Manuals", @"Technical Documents"] mutableCopy];
        if (self.catalogues) {
            self.catalogueNameArray = [NSMutableArray arrayWithCapacity:self.catalogues.count];
            for (NSDictionary *catalogue in self.catalogues) {
                [self.catalogueNameArray addObject:catalogue[@"title"]];
            }
        }
    } else {
        NSLog(@"Form:viewDidLoad actionsArray is NOT nil");

    }
    
    self.selectedActionIndex = -1;
    
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    
    NSSet *websiteDataTypes = [NSSet setWithArray:@[
                                                    WKWebsiteDataTypeDiskCache,
                                                    WKWebsiteDataTypeOfflineWebApplicationCache,
                                                    WKWebsiteDataTypeMemoryCache,
                                                    //WKWebsiteDataTypeLocalStorage,
                                                    WKWebsiteDataTypeCookies,
                                                    //WKWebsiteDataTypeSessionStorage,
                                                    //WKWebsiteDataTypeIndexedDBDatabases,
                                                    //WKWebsiteDataTypeWebSQLDatabases,
                                                    WKWebsiteDataTypeFetchCache, //(iOS 11.3, *)
                                                    //WKWebsiteDataTypeServiceWorkerRegistrations, //(iOS 11.3, *)
                                                    ]];
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
    }];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    if (!self.navigationItem.rightBarButtonItem.image)
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
    //    if (@available(iOS 13, *)) {
    //        NSLog(@"got 13");
    //    } else {
    //        NSLog(@"prior to 13");
    //        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
    //    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = self.assetMap[@"tag"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];

    NSLog(@"Form:viewWillAppear selectedActionIndex is %li", self.selectedActionIndex);
    if (self.selectedActionIndex == -1) {
        [self actionSelectPressed:self.actionButton];
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    self.homeViewController.backButton.enabled = webView.canGoBack;
}

/*
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    NSLog(@"***** did commit nav");
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    NSLog(@"***** did start prov");
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"***** failed nav *****");
}
 */

-(void)clearFormFields
{
    self.scannedAssets = [NSMutableArray array];
    self.scannedAssetIds = [NSMutableArray array];
    self.location = kLocationScanMessage;
    [self.tableView reloadData];
    for (NSDictionary *formFieldx in self.formFieldArray) {
        NSMutableDictionary *formField = [formFieldx mutableCopy];
        UITextField *textField = formField[@"textField"];
        if (textField)
            textField.text = @"";
        formField[@"fieldValue"] = nil;
    }
}

- (IBAction)submitPressed:(id)sender
{
    [self.view endEditing:YES];

    NSMutableDictionary *parameters = [@{
        @"assetId":self.assetId ,
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude),
        @"action":self.actionTagsArray[self.selectedActionIndex]
    } mutableCopy];
    
    if (self.formFieldArray.count > 0) {
        NSMutableDictionary *propertiesMap = [@{
            @"formId":self.selectedFormDict[@"formId"]
        } mutableCopy];
        
        NSMutableArray *formData = [NSMutableArray arrayWithCapacity:self.formFieldArray.count];
        
        for (NSDictionary *formField in self.formFieldArray) {
            NSMutableDictionary *fieldDict = [@{
                @"fieldType":formField[@"fieldType"],
                @"fieldKey":formField[@"fieldKey"]
            } mutableCopy];
            if ([formField[@"fieldType"] isEqualToString:@"text"] || [formField[@"fieldType"] isEqualToString:@"select"]) {
                NSString *fieldValue = formField[@"fieldValue"];
                if (fieldValue)
                    fieldDict[@"fieldValue"] = fieldValue;
            } else if ([formField[@"fieldType"] isEqualToString:@"scannedAssets"] || [formField[@"fieldType"] isEqualToString:@"associate"] || [formField[@"fieldType"] isEqualToString:@"dissociate"]) {
                fieldDict[@"fieldValues"] = self.scannedAssetIds;
            } else if ([formField[@"fieldType"] isEqualToString:@"location"]) {
                fieldDict[@"fieldValue"] = self.location;
            }
            [formData addObject:fieldDict];
        }
        propertiesMap[@"formData"] = formData;
        parameters[@"propertiesMap"] = propertiesMap;
    }
    
    [self.appDelegate postAssetAction:parameters withBlock:^(NSMutableDictionary *json)
    {
//        NSLog(@"postAssetAction json: %@", json);
        dispatch_sync(dispatch_get_main_queue(), ^{
//            [self showBannerMessage:@"Event Added" withColor:[UIColor colorFromHexString:@"a0c46d"] andImage:@"white_check" andFade:YES];
 
            [self addAction:json[@"assetHistory"]];

            [self clearFormFields];
            
            [self alertWithTitle:@"Event Added"
                      andMessage:@"Press Scan Next or Done"
                    cancelButton:@"Done"
                      andButtons:@[@"Scan Next"]
                   andTextFields:nil
                        andBlock:^(int iCount, NSArray *fields){
                                    NSLog(@"event alert, %i", iCount);
                                    switch (iCount) {
                                        case 0: // scan next
                                            [self scanPressed];
                                            break;
                                        case 1: // Done
                                            [self.navigationController popViewControllerAnimated:NO];
                                            break;
                                    }
                                }
             ];

//            [self scanPressed];
        });
    }];
}

- (IBAction)scanPressed
{
    NSLog(@"start scanning");
    [self performSegueWithIdentifier:@"QRScan" sender:self];
}

- (IBAction)actionSelectPressed:(UIButton *)button
{
    SimpleListViewController *slvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SimpleListViewController"];
    slvc.modalPresentationStyle = UIModalPresentationPopover;
    NSInteger numberOfLines = self.actionsArray.count + 1;
    slvc.actions = self.actionsArray;
    if (self.catalogueNameArray && self.catalogueNameArray.count > 0) {
        slvc.documents = self.catalogueNameArray;
        numberOfLines += slvc.documents.count + 1;
        slvc.sectionHeaders = @[@"AVAILABLE ACTIONS", @"AVAILABLE CATALOGUES"];
    } else {
        slvc.sectionHeaders = @[@"AVAILABLE ACTIONS"];
    }
    slvc.preferredContentSize = CGSizeMake(button.frame.size.width - 20, numberOfLines > 10 ? 440 : (numberOfLines * 40));
    slvc.popoverPresentationController.delegate = self;
    slvc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    slvc.popoverPresentationController.sourceView = button;
    slvc.popoverPresentationController.sourceRect = button.bounds;

    slvc.handler = self;
    slvc.selectedIndex = -1;

    [self presentViewController:slvc animated:YES completion:nil];
}

- (IBAction)formSelectPressed:(UIButton *)button
{
    SimpleListViewController *slvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SimpleListViewController"];
    slvc.modalPresentationStyle = UIModalPresentationPopover;
    
    NSMutableDictionary *formField = self.formFieldArray[button.tag];
    self.currentFormSelectButton = button;
    NSArray *selectDictArray = formField[@"formOptions"];
    NSMutableArray *selectOptions = [NSMutableArray arrayWithCapacity:selectDictArray.count];
    for (NSDictionary *option in selectDictArray) {
        if (option[@"label"])
            [selectOptions addObject:option[@"label"]];
        else
            [selectOptions addObject:option[@"fieldValue"]];
    }
    
    slvc.actions = selectOptions;
    
    slvc.preferredContentSize = CGSizeMake(button.frame.size.width - 20, selectOptions.count > 10 ? 440 : ((selectOptions.count + 1) * 40));
    slvc.popoverPresentationController.delegate = self;
    slvc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown;
    slvc.popoverPresentationController.sourceView = button;
    slvc.popoverPresentationController.sourceRect = button.bounds;

    slvc.handler = self;
    slvc.selectedIndex = -1;

    [self presentViewController:slvc animated:YES completion:nil];
}

-(void)simpleListDidCancel {}

-(void)simpleListDidSelect:(NSString *)name atIndex:(NSInteger)index andSection:(NSInteger)section
{
    NSLog(@"SLDS: %@ %li %li", name, index, section);
    
    if (self.currentFormSelectButton) {
        [self.currentFormSelectButton setTitle:name forState:UIControlStateNormal];
        
        NSMutableDictionary *formField = self.formFieldArray[self.currentFormSelectButton.tag];
        NSArray *selectDictArray = formField[@"formOptions"];
//        NSMutableDictionary *selectedDict = selectDictArray[index];
        NSDictionary *formOption = selectDictArray[index];
        formField[@"fieldValue"] = formOption[@"fieldValue"];

        self.currentFormSelectButton = nil;
    } else {
        [self.actionButton setTitle:name forState:UIControlStateNormal];
        
        if (section == 0) {
            self.formScrollView.hidden = NO;
            self.webView.hidden = YES;
            
            self.selectedActionIndex = index;
            self.scannedAssets = [NSMutableArray array];
            self.scannedAssetIds = [NSMutableArray array];
            self.location = kLocationScanMessage;
            
            self.formFieldArray = [NSMutableArray array];
            
            self.selectedFormDict = self.actionDictArray[index];
            if (self.selectedFormDict && self.selectedFormDict[@"formId"]) {
                NSString *formId = self.selectedFormDict[@"formId"];
                for (NSDictionary *form in self.appDelegate.forms) {
                    if ([formId isEqualToString:form[@"formId"]]) {
                        self.formFieldArray = [form[@"fields"] mutableCopy];
                        break;
                    }
                }
            }
            [self.tableView reloadData];
        } else {
            self.formScrollView.hidden = YES;
            self.webView.hidden = NO;
            
            NSDictionary *docDict = self.catalogues[index];
            NSString *url = docDict[@"url"];
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        }
    }
}

-(void)found:(NSString *)result withSourceTag:(NSInteger)tag viewController:(UIViewController *)viewController scanType:(NSString *)scanType objectType:(NSString*)objectType andBlock:(void(^)(NSArray *messages, bool done)) block
{
    NSLog(@"found %li", tag);
    if ([scanType isEqualToString:@"scan"]) {
        if (tag == -1) {
            self.qrResult = result;
            if ([objectType isEqualToString:AVMetadataObjectTypeQRCode]) {
                if (![self setAssetIdFromResult:self.qrResult]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self alertWithTitle:@"Error"
                                  andMessage:@"Invalid QR Code"
                                cancelButton:@"OK"
                                  andButtons:nil
                               andTextFields:nil
                                    andBlock:^(int iCount, NSArray *fields){
                            block(nil, false);
                        }
                         ];
                    });
                } else
                    [self fetchAssetScanInfoWithBlock:block];
                
                return ;
//            } else if ([objectType isEqualToString:AVMetadataObjectTypePDF417Code]) {
//                [self processExternalScanInfo:result andBlock:block];
//            } else if ([objectType isEqualToString:AVMetadataObjectTypeCode128Code]) {
//                [self processExternalScanInfo:result andBlock:block];
            }
        }
        [self processChildAssetScanInfo:result withTag:tag objectType:objectType andBlock:block];
    } else if ([scanType isEqualToString:@"external"]) {
        [self processExternalScanInfo:result withTag:tag objectType:objectType andBlock:block];
    } else if ([scanType isEqualToString:@"location"]) {
        self.location = result;
        [self.tableView reloadData];
        block(@[result], true);
    }
}

-(void)fetchAssetScanInfoWithBlock:(void(^)(NSArray *messages, bool done)) block
{
    NSString *sound = [Lockbox stringForKey:@"sound"];
    if ([@"true" isEqualToString:sound]) {
        SystemSoundID soundId;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Chime" ofType:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundId);
        AudioServicesPlaySystemSoundWithCompletion(soundId, ^{
            NSLog(@"play sound");
            AudioServicesDisposeSystemSoundID(soundId);
        });
    }
    
    NSString *vibrate = [Lockbox stringForKey:@"vibrate"];
    if ([@"true" isEqualToString:vibrate])
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

    NSMutableDictionary *postBody = [@{
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
    } mutableCopy];
    if (self.assetId)
        postBody[@"assetId"] = self.assetId;
    if (self.qrHash)
        postBody[@"qrHash"] = self.qrHash;
    
    [self.appDelegate postAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
        NSLog(@"**** asset scan result");
        dispatch_sync(dispatch_get_main_queue(), ^{
            self.assetMap = json[@"asset"];
            if (self.assetMap) {
                [self.navigationController popViewControllerAnimated:NO];
//                [self performSegueWithIdentifier:@"Form" sender:self];
                /*
                [self showSecurityElements:[json[@"isCounterfeit"] safeBoolValue]
                                                    missingQR:[json[@"missingQR"] safeBoolValue]
                                                   missingNFC:[json[@"missingNFC"] safeBoolValue]
                                                     nfcValid:[json[@"nfcValid"] safeBoolValue]
                                                      flagged:[json[@"flagged"] safeBoolValue]];
                [self updateView:json];
                 */
            } else {
                [self alertWithTitle:@"Error"
                          andMessage:@"Invalid QR Code"
                        cancelButton:@"OK"
                          andButtons:nil
                       andTextFields:nil
                            andBlock:^(int iCount, NSArray *fields){
                                block(nil, false);
                            }
                ];
            }
        });
    }];
}

-(void)processChildAssetScanInfo:(NSString *)qrResult withTag:(NSInteger)tag objectType:objectType andBlock:(void(^)(NSArray *messages, bool done)) block
{
    NSString *assetId = nil;
    NSString *qrHash = nil;
    
    if ([objectType isEqualToString:AVMetadataObjectTypeQRCode]) {
        NSArray *result = [ContentViewController parseAssetIdHashFromResult:qrResult];
        
        switch (result.count) {
            case 2:
                qrHash = result[1];
                // fall through
            case 1:
                assetId = result[0];
                break;
            case 0:
                block(@[[NSString stringWithFormat:@"Total Child Assets: %li", self.scannedAssets.count],
                        @"", @"Invalid QR Code"
                      ], false);
                return;
        }
    } else {
        assetId = qrResult;
    }
    
    if ([assetId isEqualToString:self.assetId]) {
#ifdef NotNow
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertWithTitle:@"Error"
                      andMessage:@"You many not make an Asset its own Parent"
                    cancelButton:@"OK"
                      andButtons:nil
                   andTextFields:nil
                        andBlock:nil
             ];
        });
#endif
        block(@[[NSString stringWithFormat:@"Total Child Assets: %li", self.scannedAssets.count],
                @"", @"You many not make an Asset its own Parent"
              ], false);
        return;
    }
    
    if ([self.scannedAssetIds containsObject:assetId]) {
#ifdef NotNow
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertWithTitle:@"Error"
                      andMessage:@"You have already added this Asset"
                    cancelButton:@"OK"
                      andButtons:nil
                   andTextFields:nil
                        andBlock:nil
             ];
        });
#endif
        block(@[[NSString stringWithFormat:@"Total Child Assets: %li", self.scannedAssets.count],
                @"", @"You have already added this Asset"
              ], false);
        return;
    }
    
    NSMutableDictionary *postBody = [@{
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
    } mutableCopy];
    
    postBody[@"assetId"] = assetId;
        
    if ([objectType isEqualToString:AVMetadataObjectTypeQRCode]) {
        [self.appDelegate postAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
            if (json && json[@"asset"]) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    NSDictionary *assetMap = json[@"asset"];
                    [self.scannedAssets addObject:assetMap];
                    [self.scannedAssetIds addObject:assetMap[@"assetId"]];
                    [self.tableView reloadData];
                    block(@[[NSString stringWithFormat:@"Total Child Assets: %li", self.scannedAssets.count],
                            assetMap[@"tag"]
                          ], false);
                });
            }
        }];
    } else {
        [self.appDelegate postExternalAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
            if (json && json[@"asset"]) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    NSDictionary *assetMap = json[@"asset"];
                    [self.scannedAssets addObject:assetMap];
                    [self.scannedAssetIds addObject:assetMap[@"assetId"]];
                    [self.tableView reloadData];
                    block(@[[NSString stringWithFormat:@"Total Child Assets: %li", self.scannedAssets.count],
                            assetMap[@"tag"]
                          ], true);
                });
            }
        }];
    }
}

-(void)processExternalScanInfo:(NSString *)qrResult withTag:(NSInteger)tag objectType:objectType andBlock:(void(^)(NSArray *messages, bool done)) block
{
    NSString *assetId = qrResult;

    if ([self.scannedAssetIds containsObject:assetId]) {
        block(@[[NSString stringWithFormat:@"Total Child Assets: %li", self.scannedAssets.count],
                 @"", @"You have already added this Asset"
        ], false);
        return;
    }
    
    NSMutableDictionary *postBody = [@{
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
    } mutableCopy];

    postBody[@"assetId"] = assetId;
    
    [self.appDelegate postExternalAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
        if (json && json[@"asset"]) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                NSDictionary *assetMap = json[@"asset"];
                [self.scannedAssets addObject:assetMap];
                [self.scannedAssetIds addObject:assetMap[@"assetId"]];
                [self.tableView reloadData];
                block(@[[NSString stringWithFormat:@"Total Child Assets: %li", self.scannedAssets.count],
                        assetMap[@"tag"]
                      ], true);
            });
        }
    }];
}

#define kMaxActions 25

-(void)addAction:(NSDictionary *)assetHistory
{
//    NSLog(@"events pre: %@", [self.appDelegate.events getEventsByTime]);
    NSMutableDictionary *eventRecord = [self.appDelegate.events newEvent];
    eventRecord.assetId = assetHistory[@"assetId"];
    eventRecord.assetTag = assetHistory[@"assetTag"];
    eventRecord.action = assetHistory[@"event"];
    eventRecord.assetType = assetHistory[@"assetType"];

    NSString *timeOfLogServerFormat = assetHistory[@"timeOfLog"]; // yyyyMMddHHmmss
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *gmtDate = [df dateFromString:timeOfLogServerFormat];
    NSTimeInterval gmtTime = gmtDate.timeIntervalSince1970;
    eventRecord.timeOfLog = gmtTime;
    NSInteger timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *localDate = [gmtDate dateByAddingTimeInterval:timeZoneSeconds];

    [df setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMddyyyyhhmmssa" options:0 locale:NSLocale.systemLocale]];

    eventRecord.timeOfLogString = [df stringFromDate:localDate];
//    eventRecord.timeOfLogString = assetHistory[@"timeOfLogString"];
    eventRecord.city = assetHistory[@"city"];
    eventRecord.state = assetHistory[@"state"];
    eventRecord.country = assetHistory[@"country"];
    eventRecord.postalCode = assetHistory[@"postalCode"];
    eventRecord.batchId = assetHistory[@"batchId"];
    eventRecord.productId = assetHistory[@"productId"];
    eventRecord.latitude = [assetHistory[@"latitude"] safeFloatValue];
    eventRecord.longitude = [assetHistory[@"longitude"] safeFloatValue];
    eventRecord.location = assetHistory[@"string"];
    eventRecord.facility = assetHistory[@"facility"];
    NSDictionary *propertiesMap = assetHistory[@"propertiesMap"];
    if (propertiesMap) {
        eventRecord.note = propertiesMap[@"note"];
    }
    eventRecord.propertiesMap = [propertiesMap mutableCopy];

//    NSLog(@"events post: %@", [self.appDelegate.events getEventsByTime]);

    if (self.appDelegate.events.count > kMaxActions) {   // this must be done after the timeOfVisit is set, so the new one in doesn't get trimmed!
        NSMutableArray *sortedAssets = [self.appDelegate.events getEventsByTime];
        self.appDelegate.events = [[sortedAssets subarrayWithRange:NSMakeRange(0, kMaxActions)] mutableCopy];
    }

    [self.appDelegate saveEventData];
}

#pragma mark - UITextViewDelegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.tag >= 0) {
        NSMutableDictionary *formField = self.formFieldArray[textView.tag];
        formField[@"fieldValue"] = textView.text;
    }
}

#pragma mark - UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSMutableDictionary *formField = [self.formFieldArray[indexPath.section] mutableCopy];
    if ([formField[@"fieldType"] isEqualToString:@"text"]) {
        FormTableTextEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableTextEntry"];
        cell.tag = indexPath.section;
        cell.textEntry.backgroundColor = [UIColor whiteColor];
        cell.textEntry.tag = indexPath.section;
        cell.textEntry.delegate = self;
        formField[@"textField"] = cell.textEntry;
        return cell;
    } else if ([formField[@"fieldType"] isEqualToString:@"select"]) {
        FormTableSelectEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableSelectEntry"];
        cell.selectButton.tag = indexPath.section;
//        cell.uvc = self;
//        cell.formField = formField;
 //       cell.textEntry.tag = indexPath.section;
//        cell.textEntry.delegate = self;
//        formField[@"textField"] = cell.textEntry;
        return cell;
    } else if ([formField[@"fieldType"] isEqualToString:@"scannedAssets"]) {
        if (indexPath.row == 0) {
            FormTableAssetsEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableAssets"];
            cell.numAdded.text = [NSString stringWithFormat:@"%li added", self.scannedAssets.count];
            cell.tag = indexPath.section;
            cell.fvc = self;
            return cell;
        }
        FormTableAssetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableAsset"];
        NSDictionary *asset = self.scannedAssets[indexPath.row - 1];
        cell.label.text = asset[@"tag"];
        return cell;
    } else if ([formField[@"fieldType"] isEqualToString:@"associate"] || [formField[@"fieldType"] isEqualToString:@"dissociate"]) {
        if (indexPath.row == 0) {
            FormTableExternalEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableExternal"];
            cell.numAdded.text = [NSString stringWithFormat:@"%li added", self.scannedAssets.count];
            cell.tag = indexPath.section;
            cell.fvc = self;
            return cell;
        }
        FormTableAssetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableAsset"];
        NSDictionary *asset = self.scannedAssets[indexPath.row - 1];
        cell.label.text = asset[@"tag"];
        return cell;
    } else if ([formField[@"fieldType"] isEqualToString:@"location"]) {
//        if (indexPath.row == 0) {
            FormTableLocationEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableLocation"];
            cell.binLocation.text = self.location;
            cell.tag = indexPath.section;
            cell.fvc = self;
            return cell;
//        }
        /*
        FormTableAssetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormTableAsset"];
        NSDictionary *asset = self.scannedAssets[indexPath.row - 1];
        cell.label.text = asset[@"tag"];
        return cell;
         */
    }

    return nil;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableDictionary *formField = self.formFieldArray[section];
    if ([formField[@"fieldType"] isEqualToString:@"scannedAssets"] || [formField[@"fieldType"] isEqualToString:@"location"] || [formField[@"fieldType"] isEqualToString:@"associate"] || [formField[@"fieldType"] isEqualToString:@"dissociate"]) {
        return 1 + self.scannedAssets.count;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.formFieldArray.count;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FormSectionHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormSectionHeader"];
//    cell.backgroundColor = [UIColor colorFromHexString:@"ffffff80"];
    NSMutableDictionary *formField = self.formFieldArray[section];
    cell.label.text = formField[@"label"];
    cell.tag = section;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    /*
    if (section + 1 == self.formFieldArray.count) { // only for the last section
        FormSectionFooterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FormSectionFooter"];
        return cell;
    }
     */
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *formField = self.formFieldArray[indexPath.section];
    if ([formField[@"fieldType"] isEqualToString:@"scannedAssets"] || [formField[@"fieldType"] isEqualToString:@"location"] || [formField[@"fieldType"] isEqualToString:@"associate"] || [formField[@"fieldType"] isEqualToString:@"dissociate"]) {
        if (indexPath.row == 0)
            return 70;
        return 24;
    }
    if ([formField[@"fieldType"] isEqualToString:@"text"]) {
        return 100;
    }
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 24;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    /*
    if (section + 1 == self.formFieldArray.count) { // only for the last section
        return 80;
    }
     */
    return 0;
}

#pragma mark - UIStoryboardSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"QRScan" isEqualToString:segue.identifier]) {
        QRScannerViewController *qrvc = segue.destinationViewController;
        qrvc.handler = self;
        qrvc.scanType = @"scan";
        if (sender == self) {
            qrvc.sourceTag = -1;
        } else {
            qrvc.sourceTag = ((UIView *)sender).tag;
        }
    } else if ([@"ExternalScan" isEqualToString:segue.identifier]) {
        QRScannerViewController *qrvc = segue.destinationViewController;
        qrvc.handler = self;
        qrvc.sourceTag = ((UIView *)sender).tag;
        qrvc.scanType = @"external";
    } else if ([@"LocationScan" isEqualToString:segue.identifier]) {
        QRScannerViewController *qrvc = segue.destinationViewController;
        qrvc.handler = self;
        qrvc.sourceTag = ((UIView *)sender).tag;
        qrvc.scanType = @"location";
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

#ifdef NotNow

-(void)closePickerView:(id)sender
{
    [self.currentPickerTextField endEditing:YES];
    
    if (self.currentPickerTextField.tag == -1) {    // actions picker
        self.selectedActionIndex = self.pickerSelectionIndex;
        self.scannedAssets = [NSMutableArray array];
        self.scannedAssetIds = [NSMutableArray array];
        
        self.formFieldArray = [NSMutableArray array];
        
        self.selectedFormDict = self.actionDictArray[self.pickerSelectionIndex];
        if (self.selectedFormDict && self.selectedFormDict[@"formId"]) {
            NSString *formId = self.selectedFormDict[@"formId"];
            for (NSDictionary *form in self.appDelegate.forms) {
                if ([formId isEqualToString:form[@"formId"]]) {
                    self.formFieldArray = [form[@"fields"] mutableCopy];
                    [self.tableView reloadData];
                    break;
                }
            }
        }
        [self.tableView reloadData];
    }
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.currentPickerTextField = textField;
    
    if (textField.tag >= 0) {  // if it's not the action picker, it's a formOptions picker
        NSMutableDictionary *formField = self.formFieldArray[textField.tag];
        if ([formField[@"fieldType"] isEqualToString:@"select"]) {
            textField.inputView = self.pickerView;
            textField.inputAccessoryView = self.pickerToolbar;
            
            NSDictionary *formOptions = formField[@"formOptions"];
            self.formOptionValues = [NSMutableArray arrayWithCapacity:formOptions.count];
            self.formDictArray = [NSMutableArray arrayWithCapacity:formOptions.count];
            for (NSDictionary *formOption in formOptions) {
                [self.formOptionValues addObject:formOption[@"label"]];
                [self.formDictArray addObject:formOption];
            }
        }
    } else {
        [self.pickerView selectRow:self.selectedActionIndex inComponent:0 animated:YES];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    if (textField.tag >= 0) {
        NSMutableDictionary *formField = self.formFieldArray[textField.tag];
        formField[@"fieldValue"] = textField.text;
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.currentPickerTextField.tag == -1) {
        return self.actionsArray.count;
    }
    return self.formOptionValues.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.currentPickerTextField.tag == -1) {
        return self.actionsArray[row];
    }
    return self.formOptionValues[row];
}

#pragma mark - UIPickerViewDelegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.pickerSelectionIndex = row;
    
    if (self.currentPickerTextField.tag == -1) {
        self.currentPickerTextField.text = self.actionsArray[row];
    } else {
        NSDictionary *formOptionDict = self.formDictArray[row];
        self.currentPickerTextField.text = formOptionDict[@"fieldValue"];
        
    }
}
#endif

@end
