//
//  HistoryViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "HistoryViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Data.h"
#import "NSMutableArray+Events.h"
#import "NSMutableDictionary+Event.h"
#import "NSCombined+Values.h"
#import "UIColor+Convert.h"
#import "TableFooterView.h"
#import "HistoryDetailViewController.h"

@interface EventActionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeOfLog;
@property (weak, nonatomic) IBOutlet UILabel *assetTag;
@property (weak, nonatomic) IBOutlet UILabel *action;
@property (weak, nonatomic) IBOutlet UILabel *city;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *country;
@property (weak, nonatomic) IBOutlet UILabel *postalCode;

@property (nonatomic) NSMutableDictionary *event;

@end

@implementation EventActionCell

-(void)setData:(NSMutableDictionary *)event withColor:(UIColor *)color
{
    self.event = event;
    
    NSLog(@"timeOfLog: %@ = %i", event.timeOfLogString, event.timeOfLog);
    self.timeOfLog.text = event.timeOfLogString;
    self.assetTag.text = event.assetTag;
    self.action.text = event.action;
    self.city.text = event.city;
    self.state.text = event.state;
    self.country.text = event.country;
    self.postalCode.text = event.postalCode;
    
    self.action.textColor = color;
    self.assetTag.textColor = color;
    self.city.textColor = color;
    self.state.textColor = color;
    self.country.textColor = color;
    self.postalCode.textColor = color;
}

@end

@interface EventSectionHeaderCell : UITableViewCell

@end

@implementation EventSectionHeaderCell

@end

@interface HistoryViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableArray *eventList;

@end

@implementation HistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.eventList = [self.appDelegate.events getEventsByTime];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
//    self.tableView.backgroundColor = [UIColor colorFromHexString:@"32355C"];
    self.tableView.tableFooterView = [[TableFooterView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];
    
    [self.tableView reloadData];
}

- (void)backPressed
{
}

-(void)afterActivation
{
//    self.homeViewController.backButton.enabled = NO;
}

#pragma mark - UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSMutableDictionary *eventDict = self.eventList[indexPath.row];
    EventActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventAction"];
    [cell setData:eventDict withColor:self.appDelegate.text2Color];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = self.eventList.count;
    [(TableFooterView *)self.tableView.tableFooterView setHasData:(numberOfRows > 0) alternateMessage:@"no events available"];
    return numberOfRows;
}

#pragma mark - UITableViewDelegate

#ifdef NotNow
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    EventSectionHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventSectionHeader"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}
#endif

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"ShowHistoryDetail" isEqualToString:segue.identifier]) {
        HistoryDetailViewController *hdvc = segue.destinationViewController;
        EventActionCell *cell = (EventActionCell *)sender;

        hdvc.event = cell.event;
//        hdvc.homeViewController = self.homeViewController;
    }

}

@end
