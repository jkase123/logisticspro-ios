//
//  FormViewController.h
//  LX Connect
//
//  Created by Jeff Kase on 4/9/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

#import "ContentViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FormViewController : ContentViewController

@property (nonatomic) NSMutableDictionary *assetMap;
@property (nonatomic) NSArray *catalogues;

@end

NS_ASSUME_NONNULL_END
