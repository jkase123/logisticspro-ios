//
//  HomeViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "HomeViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "NSCombined+Values.h"
#import "UIColor+Convert.h"
#import "UIViewController+Alert.h"
#import "AlertViewController.h"
#import "Lockbox.h"
#import "ContentViewController.h"

@interface HomeViewController () <UIPopoverPresentationControllerDelegate, UITabBarDelegate /*, HintChange */>

@property (weak, nonatomic) IBOutlet UIButton *hintButton;
@property (weak, nonatomic) IBOutlet UIView *securityBackground;
@property (weak, nonatomic) IBOutlet UIImageView *securityImage;
@property (weak, nonatomic) IBOutlet UILabel *securityText;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic) ContentViewController *scanViewController, *historyViewController, *settingsViewController;
@property (weak) ContentViewController *currentViewController;
@property NSInteger currentTag;
//@property bool showFormController;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.backButton.enabled = NO;
    self.scanMode = @"QR Code Only";

    [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex:0]];
    self.tabBar.delegate = self;
    
    self.securityBackground.hidden = YES;
    [self.securityBackground setAlpha:0.0f];
    
    // defaults for the settings
    //
    NSString *result = [Lockbox stringForKey:@"sound"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"sound"];
    
    result = [Lockbox stringForKey:@"vibrate"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"vibrate"];
    
    result = [Lockbox stringForKey:@"history"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"history"];
    
    result = [Lockbox stringForKey:@"hideMenu"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"hideMenu"];
    //

//    self.showFormController = NO;
    
    self.scanViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanViewController"];
    self.scanViewController.homeViewController = self;
    
    self.historyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
    self.historyViewController.homeViewController = self;
    
    self.settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    self.settingsViewController.homeViewController = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.currentViewController = self.scanViewController;
    self.currentTag = 0;
    
    [self displayContentController:self.scanViewController];

    /*
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [session setActive:YES error:nil];
     */
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editAsset:) name:kEditAsset object:nil];
}
/*
-(void)editAsset:(NSNotification *)notification
{
    self.assetId = notification.userInfo[@"assetId"];
}
*/
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) displayContentController:(UIViewController *)content
{
    [self addChildViewController:content];
    content.view.frame = self.contentView.bounds;
    [self.contentView addSubview:content.view];
    [content didMoveToParentViewController:self];
}

- (void)cycleFromViewController: (UIViewController*) oldVC
               toViewController: (UIViewController*) newVC
                         isLeft:(bool)isLeft
{
    // Prepare the two view controllers for the change.
    [oldVC willMoveToParentViewController:nil];
    [self addChildViewController:newVC];
    
    CGRect rc = self.contentView.bounds;
    CGRect endFrame = self.contentView.bounds;
    
    if (isLeft) {
        rc.origin.x -= rc.size.width;
        newVC.view.frame = rc;
        endFrame.origin.x += endFrame.size.width;
    } else {
        rc.origin.x += rc.size.width;
        newVC.view.frame = rc;        
        endFrame.origin.x -= endFrame.size.width;
    }
    
    // Queue up the transition animation.
    [self transitionFromViewController: oldVC toViewController: newVC
                              duration: 0.25 options:0
                            animations:^{
        // Animate the views to their final positions.
        newVC.view.frame = oldVC.view.frame;
        oldVC.view.frame = endFrame;
    }
                            completion:^(BOOL finished) {
        // Remove the old view controller and send the final
        // notification to the new view controller.
        [oldVC removeFromParentViewController];
        [newVC didMoveToParentViewController:self];
    }];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    ContentViewController *newViewController = nil;
    NSInteger newTag = item.tag;
    
    switch (item.tag) {
        case 0:
            newViewController = self.scanViewController;
            break;
        case 1:
            newViewController = self.historyViewController;
            break;
        case 2:
            newViewController = self.settingsViewController;
            break;
    }
    if (newViewController != self.currentViewController) {
        [self cycleFromViewController:self.currentViewController toViewController:newViewController isLeft:(newTag < self.currentTag)];
        
        self.currentViewController = newViewController;
        self.currentTag = newTag;
        
        [self.currentViewController afterActivation];
    }
}

- (IBAction)backPressed:(id)sender
{
    if ([self.currentViewController respondsToSelector:@selector(backPressed)])
        [self.currentViewController backPressed];
}

-(void)showBannerMessage:(NSString *)message withColor:(UIColor *)color andImage:(NSString *)image andFade:(bool)fade
{
    self.securityBackground.hidden = YES;
    [self.securityBackground setAlpha:0.0f];
    
    self.securityBackground.backgroundColor = color;
    self.securityText.text = message;
    self.securityImage.image = [UIImage imageNamed:image];
    
    self.securityBackground.hidden = NO;
    [self.view bringSubviewToFront:self.securityBackground];

    if (fade) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            // fade in
            [self.securityBackground setAlpha:1.0f];
        } completion:^(BOOL finished) {
            //fade out
            [UIView animateWithDuration:1.0 delay:2.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                [self.securityBackground setAlpha:0.0f];
            } completion:nil];
        }];
    } else
        [self.securityBackground setAlpha:1.0f];
}

-(void)showSecurityBannerOK:(bool)ok withHidden:(bool)hidden fade:(bool)fade
{
    self.securityBackground.hidden = YES;
    [self.securityBackground setAlpha:0.0f];
    if (hidden)
        return;
    
    if (ok) {
        [self showBannerMessage:@"Valid" withColor:[UIColor colorFromHexString:@"a0c46d"] andImage:@"white_check" andFade:fade];
    } else {
        [self showBannerMessage:@"WARNING" withColor:[UIColor colorFromHexString:@"eb4f35"] andImage:@"white_x" andFade:fade];
    }
}

-(void)switchToFormWithData:(NSMutableDictionary *)json
{
    self.scanViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FormViewController"];
    self.scanViewController.homeViewController = self;
    
//    self.showFormController = YES;

    [self displayContentController:self.scanViewController];
    [self.scanViewController updateView:json];
    self.currentViewController = self.scanViewController;
    [self.currentViewController afterActivation];
}

-(void)switchToScan
{
    self.scanViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanViewController"];
    self.scanViewController.homeViewController = self;
    
//    self.showFormController = YES;

    [self displayContentController:self.scanViewController];
//    [self.scanViewController updateView:json];
    self.currentViewController = self.scanViewController;
    [self.currentViewController afterActivation];
}

-(bool)setAssetIdFromResult:(NSString *)url
{
    self.assetId = nil;
    self.qrHash = nil;
    
    NSArray *result = [HomeViewController parseAssetIdHashFromResult:url];
    switch (result.count) {
        case 2:
            self.qrHash = result[1];
            // fall through
        case 1:
            self.assetId = result[0];
            return true;
    }
    return false;
}

+(NSArray *)parseAssetIdHashFromResult:(NSString *)url
{
    NSURL *resultURL = [NSURL URLWithString:url];
    NSLog(@"url: %@", resultURL.query);
    NSString *assetId = nil;
    NSString *hash = nil;
    
    if (resultURL.query && resultURL.query.length > 5) {
        NSArray *urlComponents = [resultURL.query componentsSeparatedByString:@"&"];
        for (NSString *keyPair in urlComponents) {
            NSArray *tagComponents = [keyPair componentsSeparatedByString:@"="];
            if ([tagComponents[0] isEqualToString:@"assetId"]) {
                assetId = tagComponents[1];
            } else if ([tagComponents[0] isEqualToString:@"hash"]) {
                hash = tagComponents[1];
            }
        }
    }
    if (!assetId) {
        for (NSString *path in resultURL.pathComponents) {
            NSLog(@"  path: %@", path);
            if ([[NSUUID alloc] initWithUUIDString:path]) {
                assetId = path;
            }
        }
    }

    NSLog(@"assetId: %@ hash: %@", assetId, hash);
    if (assetId && hash)
        return @[assetId, hash];
    
    if (assetId)
        return @[assetId];
    
    return @[];
}

-(bool)showSecurityElements:(BOOL)isCounterfeit missingQR:(BOOL)missingQR missingNFC:(BOOL)missingNFC nfcValid:(BOOL)nfcValid flagged:(BOOL)flagged
{
    BOOL showAlert = NO;
    UIColor *alertBackgroundColor;
    NSString *alertText;
    bool valid = YES;
    
    if (isCounterfeit || flagged) {
        valid = NO;
    } else {
        if (nfcValid) {
            if (missingQR && ![self.scanMode isEqualToString:@"NFC Only"]) {
                valid = NO;
            }
        }
        if (missingNFC && ![self.scanMode isEqualToString:@"QR Code Only"]) {
            valid = NO;
        }
    }
    if (!missingNFC && !nfcValid) {
        valid = NO;
    }

    if (!valid)
        [self showSecurityBannerOK:valid withHidden:NO fade:YES];
    
    if (showAlert) {
        AlertViewController *avc = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertPopup"];
        avc.modalPresentationStyle = UIModalPresentationPopover;
        avc.preferredContentSize = CGSizeMake(300, 80);
        avc.view.backgroundColor = alertBackgroundColor;
        avc.popoverPresentationController.delegate = self;
        avc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        avc.popoverPresentationController.sourceView = self.backButton;
        avc.popoverPresentationController.sourceRect = self.backButton.bounds;
        avc.label.text = alertText;
        
        [self presentViewController:avc animated:YES completion:nil];
        
//        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    }
    return valid;
}

int nextIndex = -1;
bool securityBannerWasHidden = NO;

- (IBAction)hintPressed:(id)sender
{
    [self.appDelegate logout];

#ifdef NotNow
    nextIndex = 1;
    
    [self configureHint];
#endif
}

-(void)configureHint
{
#ifdef NotNow
    HintViewController *hvc = [self.storyboard instantiateViewControllerWithIdentifier:@"HintPopup"];
    hvc.modalPresentationStyle = UIModalPresentationPopover;
    hvc.preferredContentSize = CGSizeMake(300, 140);
    hvc.view.backgroundColor = [UIColor colorFromHexString:@"009ed2"];
    hvc.popoverPresentationController.delegate = self;
    hvc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    hvc.index = nextIndex;
    hvc.handler = self;

    switch (nextIndex) {
        case 1:
            hvc.popoverPresentationController.sourceView = self.backButton;
            hvc.popoverPresentationController.sourceRect = self.backButton.bounds;
            hvc.label.text = @"Click here to go to the previous page";
            break;
        case 2:
            hvc.popoverPresentationController.sourceView = self.securityBackground;
            hvc.popoverPresentationController.sourceRect = self.securityBackground.bounds;
            hvc.label.text = @"This bar will show any security risks for your recent scan";
            if (self.securityBackground.isHidden) {
                [self showSecurityBannerOK:YES withHidden:NO fade:NO];
                securityBannerWasHidden = YES;
                [self.view bringSubviewToFront:self.securityBackground];
            }
            break;
        case 3:
        {
//            UIView *view = [[self.tabBar.items objectAtIndex:0] valueForKey:@"view"];
            hvc.popoverPresentationController.sourceView = self.tabBar;
            hvc.popoverPresentationController.sourceRect = self.tabBar.bounds;
            hvc.label.text = @"Click here to go to Scan a QR Code, see your recent Scans, or adjust your Settings";
            hvc.nextButton.hidden = YES;
        }
            break;
    }

    [self presentViewController:hvc animated:YES completion:nil];
#endif
}

- (void)hintChanged:(int)select
{
    if (securityBannerWasHidden) {
        [self showSecurityBannerOK:YES withHidden:YES fade:NO];
    }
    nextIndex = select;
    if (nextIndex > 0)
        [self configureHint];
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    if (securityBannerWasHidden) {
        [self showSecurityBannerOK:YES withHidden:YES fade:NO];
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}

@end
