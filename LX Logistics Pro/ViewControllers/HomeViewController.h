//
//  HomeViewController.h
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, nullable) NSString *assetId, *scanMode, *qrHash;

-(void)showBannerMessage:(NSString *)message withColor:(UIColor *)color andImage:(NSString *)image andFade:(bool)fade;
-(bool)showSecurityElements:(BOOL)isCounterfeit missingQR:(BOOL)missingQR missingNFC:(BOOL)missingNFC nfcValid:(BOOL)nfcValid flagged:(BOOL)flagged;
-(void)switchToFormWithData:(NSDictionary *)json;
-(bool)setAssetIdFromResult:(NSString *)url;
+(NSArray *)parseAssetIdHashFromResult:(NSString *)url;
-(void)switchToScan;

@end

NS_ASSUME_NONNULL_END
