//
//  LoginViewController.m
//  Locator
//
//  Created by Jeff Kase on 4/4/16.
//  Copyright © 2016 LocatorX. All rights reserved.
//

#import "LoginViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+UrlSession.h"
#import "AlertViewController.h"
#import "UIViewController+Alert.h"

@interface LoginViewController () <UITextFieldDelegate, UIPopoverPresentationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *privacyPolicy;
@property (weak, nonatomic) IBOutlet UIButton *termsOfUse;
@property (weak, nonatomic) IBOutlet UIView *verticalDivider;
@property (weak, nonatomic) IBOutlet UIImageView *largeIcon;
@property (weak, nonatomic) IBOutlet UIView *emailBackground;
@property (weak, nonatomic) IBOutlet UIView *passwordBackground;

@end

#ifdef STAGING
    #define kForgotPasswordUrl @"https://lxconnect-staging.locatorx.com/forgotPassword"
#else
    #ifdef DEV
        #define kForgotPasswordUrl @"https://lxconnect-dev.locatorx.com/forgotPassword"
    #else
        #define kForgotPasswordUrl @"https://lxconnect.locatorx.com/forgotPassword"
    #endif
#endif

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self hideLoginElements:self.appDelegate.hasCredentials];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (self.appDelegate.hasCredentials) {
        [self.appDelegate loginWithBlock:^(NSMutableDictionary *json) {
            if (!json[@"user"])
                [self hideLoginElements:NO];
        }];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.appDelegate.hasCredentials) {
        float scale = self.view.frame.size.width / self.largeIcon.frame.size.width;
        [UIView animateWithDuration:1.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.largeIcon.center = self.view.center;
            self.largeIcon.transform = CGAffineTransformMakeScale(scale, scale);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.3 delay:2.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                self.largeIcon.alpha = 0.0f;
            } completion:^(BOOL finished) {
                [self.appDelegate setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
            }];
        }];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)hideLoginElements:(BOOL)hide
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.emailField.hidden = hide;
        self.passwordField.hidden = hide;
        self.loginButton.hidden = hide;
        self.emailLabel.hidden = hide;
        self.passwordLabel.hidden = hide;
        self.forgotPassword.hidden = hide;
        self.privacyPolicy.hidden = hide;
        self.termsOfUse.hidden = hide;
        self.verticalDivider.hidden = hide;
        self.emailBackground.hidden = hide;
        self.passwordBackground.hidden = hide;
    });
}

- (IBAction)loginPressed:(UIButton *)sender
{
    [self.view endEditing:YES];

    if ([self validateEmailAndPassword]) {
        [self.appDelegate login:self.emailField.text
                   withPassword:self.passwordField.text
                       andBlock:^(NSMutableDictionary *json){
            if (!json || !json[@"user"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self alertWithTitle:@"Error"
                              andMessage:json[@"error"]
                            cancelButton:@"OK"
                              andButtons:nil
                           andTextFields:nil
                                andBlock:nil];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.appDelegate setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
                });
            }
        }];
    }
        
}

-(bool)validateEmailAndPassword
{
    if (self.emailField.text.length < 5) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertWithTitle:@"Error"
                      andMessage:@"Please enter a valid Email address"
                    cancelButton:@"OK"
                      andButtons:nil
                   andTextFields:nil
                        andBlock:nil];
        });
        return NO;
    }
    
    if (self.passwordField.text.length < 5) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertWithTitle:@"Error"
                      andMessage:@"Please enter a valid Password (min 5 chars)"
                    cancelButton:@"OK"
                      andButtons:nil
                   andTextFields:nil
                        andBlock:nil];
        });
        return NO;
    }
    
    return YES;
}

-(bool)validateEmail
{
    if (self.emailField.text.length < 5) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertWithTitle:@"Error"
                      andMessage:@"Please enter a valid Email address"
                    cancelButton:@"OK"
                      andButtons:nil
                   andTextFields:nil
                        andBlock:nil];
        });
        return NO;
    }
    
    return YES;
}

- (IBAction)forgotPasswordPressed:(UIButton *)sender
{
#ifdef NotNow
    [self.view endEditing:YES];

    if ([self validateEmail]) {
    
        [self.appDelegate forgotPasswordRequest:self.emailField.text
                                      withBlock:^(NSMutableDictionary *json){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self alertWithTitle:@"Success"
                          andMessage:@"An Email has been Sent"
                        cancelButton:@"OK"
                          andButtons:nil
                       andTextFields:nil
                            andBlock:nil];
            });
        }];
    }
#else
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kForgotPasswordUrl] options:@{} completionHandler:nil];
#endif
}

- (IBAction)privacyPolicyPressed:(UIButton *)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.appDelegate.privacyPolicy] options:@{} completionHandler:nil];
}

- (IBAction)termsOfUsePressed:(UIButton *)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.appDelegate.termsOfUse] options:@{} completionHandler:nil];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailField) {
        [self.passwordField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self loginPressed:nil];
    }
    return YES;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
