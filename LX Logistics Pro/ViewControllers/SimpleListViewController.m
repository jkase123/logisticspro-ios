//
//  SimpleListViewController.m
//  RestorationLibrary
//
//  Created by Jeff Kase on 1/8/15.
//  Copyright (c) 2015 Jeff Kase. All rights reserved.
//

#import "SimpleListViewController.h"
#import "UIColor+Convert.h"

@interface SimpleListViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *greenCheck;

@end

@implementation SimpleListViewCell

-(void)setData:(NSString *)name selected:(NSString *)selected
{
    self.title.text = name;
    self.greenCheck.hidden = ![name isEqualToString:selected];
}

@end

@interface SectionHeaderViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation SectionHeaderViewCell

-(void)setData:(NSString *)name
{
    self.title.text = name;
}

@end

@interface SimpleListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SimpleListViewController

+(int)rowHeight
{
    return 40;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
//    self.tableView.backgroundColor = [UIColor colorFromHexString:@"555A9C"];
}

#pragma mark - Table view data source

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionHeaderViewCell *header = [tableView dequeueReusableCellWithIdentifier:@"SectionHeader"];
    
    if (!self.sectionHeaders || self.sectionHeaders.count == 0)
        header.title.text = @"";
    else
        header.title.text = self.sectionHeaders[section];
//    header.backgroundColor = [UIColor colorFromHexString:@"a0c46d"];

    return header;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!self.sectionHeaders || self.sectionHeaders.count == 0)
        return 1;
    
    return self.sectionHeaders.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SimpleListViewController rowHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [self.actions count];
        case 1:
            return [self.documents count];
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selection = @"";
    
    switch (indexPath.section) {
        case 0:
            selection = self.actions[indexPath.row];
            break;
        case 1:
            selection = self.documents[indexPath.row];
            break;
    }
    
    if (self.handler && [self.handler respondsToSelector:@selector(simpleListDidSelect:atIndex:andSection:)]) {
        [self.handler simpleListDidSelect:selection atIndex:indexPath.row andSection:indexPath.section];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SimpleListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleListViewCell" forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:
            [cell setData:self.actions[indexPath.row] selected:(self.selectedIndex >= 0 ? self.actions[self.selectedIndex] : @"")];
            break;
        case 1:
            [cell setData:self.documents[indexPath.row] selected:@""];
            break;
    }

    return cell;
}

@end
