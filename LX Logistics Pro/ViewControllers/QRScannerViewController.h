//
//  QRScannerViewController.h
//  SmartLabelDemo
//
//  Created by Jeff Kase on 9/5/17.
//  Copyright © 2017 LocatorX. All rights reserved.
//

@import UIKit;

@protocol QRScannerResult <NSObject>

@optional
-(void)found:(NSString *)result withSourceTag:(NSInteger)tag viewController:(UIViewController *)viewController scanType:(NSString *)scanType objectType:(NSString*)objectType andBlock:(void(^)(NSArray *messages, bool done)) block;

@end

@interface QRScannerViewController : UIViewController

@property (nonatomic) id<QRScannerResult> handler;
@property NSInteger sourceTag;
@property (nonatomic) NSString *scanType;

@end
