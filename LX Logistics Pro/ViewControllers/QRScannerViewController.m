//
//  QRScannerViewController.m
//  SmartLabelDemo
//
//  Created by Jeff Kase on 9/5/17.
//  Copyright © 2017 LocatorX. All rights reserved.
//

@import AVFoundation;

#import "QRScannerViewController.h"
#import "UIViewController+AppDelegate.h"

@interface QRScannerViewController () <AVCaptureMetadataOutputObjectsDelegate>

@property (weak, nonatomic) IBOutlet UIView *topBar;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalChildAssets;
@property (weak, nonatomic) IBOutlet UIView *totalChildAssetsBackground;
@property (weak, nonatomic) IBOutlet UIButton *countdownButton;

@property (nonatomic) AVCaptureDevice            *defaultDevice;
@property (nonatomic) AVCaptureDeviceInput       *defaultDeviceInput;
@property (nonatomic) AVCaptureDevice            *frontDevice;
@property (nonatomic) AVCaptureDeviceInput       *frontDeviceInput;
@property (nonatomic) AVCaptureMetadataOutput    *metadataOutput;
@property (nonatomic) AVCaptureSession           *captureSession;
@property (nonatomic) AVCaptureVideoPreviewLayer *previewLayer;

@property int timerTicks;

@end

@implementation QRScannerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];

    self.defaultDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    self.countdownButton.backgroundColor = self.appDelegate.buttonColor;
//    self.totalChildAssets.textColor = self.appDelegate
    self.messageLabel.textColor = UIColor.blackColor;
    self.messageLabel.backgroundColor = self.appDelegate.text1Color;

    NSError *error;
    self.defaultDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.defaultDevice error:&error];
    self.metadataOutput     = [[AVCaptureMetadataOutput alloc] init];
    [self.metadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    self.captureSession     = [[AVCaptureSession alloc] init];
    
    [self.captureSession addInput:self.defaultDeviceInput];
    [self.captureSession addOutput:self.metadataOutput];
    
    self.metadataOutput.metadataObjectTypes = @[AVMetadataObjectTypeQRCode,AVMetadataObjectTypePDF417Code,
                                                AVMetadataObjectTypeCode128Code,AVMetadataObjectTypeUPCECode,
                                                AVMetadataObjectTypeCode39Code,AVMetadataObjectTypeCode39Mod43Code,
                                                AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeEAN8Code,
                                                AVMetadataObjectTypeCode93Code,AVMetadataObjectTypeAztecCode,
                                                AVMetadataObjectTypeInterleaved2of5Code,AVMetadataObjectTypeITF14Code,
                                                AVMetadataObjectTypeDataMatrixCode];

    self.previewLayer       = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view.layer addSublayer:self.previewLayer];
    self.previewLayer.frame = self.view.layer.bounds;
    
    [self.view bringSubviewToFront:self.topBar];
    [self.view bringSubviewToFront:self.messageLabel];
    
    [self.view bringSubviewToFront:self.totalChildAssetsBackground];
    [self.view bringSubviewToFront:self.totalChildAssets];
    
    self.totalChildAssetsBackground.hidden = self.sourceTag == -1;
    self.totalChildAssets.hidden = self.sourceTag == -1;
    self.countdownButton.hidden = YES;

    [self.captureSession startRunning];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)closeButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AVCaptureMetadataOutputObjects Delegate Methods

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    for (AVMetadataObject *current in metadataObjects) {
        if ([current isKindOfClass:[AVMetadataMachineReadableCodeObject class]]) {
            NSString *scannedResult = [(AVMetadataMachineReadableCodeObject *)current stringValue];
//            NSLog(@"result: %@", scannedResult);
//            self.messageLabel.text = scannedResult;
            [self.captureSession stopRunning];
            
            if (self.handler && [self.handler respondsToSelector:@selector(found:withSourceTag:viewController:scanType:objectType:andBlock:)]) {
                [self.handler found:scannedResult withSourceTag:self.sourceTag viewController:self scanType:self.scanType objectType:[(AVMetadataMachineReadableCodeObject *)current type] andBlock:^(NSArray* messages, bool done) {
                    if (messages && messages.count >= 2) {
                        self.totalChildAssets.text = messages[0];
                        if (messages.count == 3) {
                            self.messageLabel.text = messages[2];
                            self.messageLabel.textColor = UIColor.whiteColor;
                            self.messageLabel.backgroundColor = self.appDelegate.text2Color;
                        } else {
                            self.messageLabel.text = messages[1];
                            self.messageLabel.textColor = UIColor.blackColor;
                            self.messageLabel.backgroundColor = self.appDelegate.text1Color;
                        }
                        if (done) {
                            [self dismissViewControllerAnimated:YES completion:^{
                                [self.navigationController popViewControllerAnimated:YES];
//                                [[NSNotificationCenter defaultCenter] postNotificationName:kScannerClosed object:nil];
                            }];
                        } else {
                            NSLog(@"receiving found scan item");
                            self.countdownButton.hidden = NO;
                            [self.countdownButton setTitle:@"Tap if Done.  Continuing in 5 secs" forState:UIControlStateNormal];
                            [self.view bringSubviewToFront:self.countdownButton];
                            self.timerTicks = 5;
                            
                            [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer *timer) {
                                if (--self.timerTicks <= 0) {
                                    NSLog(@"done with timer");
                                    [timer invalidate];
                                    self.countdownButton.hidden = YES;
                                    self.messageLabel.text = @"Aim your camera at a QR Code";
                                    self.messageLabel.textColor = UIColor.blackColor;
                                    self.messageLabel.backgroundColor = self.appDelegate.text1Color;
                                    [self.captureSession startRunning];
                                } else {
                                    [UIView performWithoutAnimation:^{
                                        [self.countdownButton setTitle:[NSString stringWithFormat:@"Tap if Done.  Continuing in %i secs", self.timerTicks] forState:UIControlStateNormal];
                                        [self.countdownButton layoutIfNeeded];
                                    }];
                                }
                            }];
//                        [self.captureSession startRunning];
                        }
                    } else {
                        if (!done) {
                            [self.captureSession startRunning];
                        } else {
                            [self dismissViewControllerAnimated:YES completion:^{
                                [self.navigationController popViewControllerAnimated:YES];
//                                [[NSNotificationCenter defaultCenter] postNotificationName:kScannerClosed object:nil];
                            }];
                        }
                    }
                }];
            }
            
            break;
        }
    }
}

@end
