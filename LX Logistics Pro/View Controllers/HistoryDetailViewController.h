//
//  HistoryDetailViewController.h
//  LX Logistics Pro
//
//  Created by Jeff Kase on 9/23/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface HistoryDetailViewController : UITableViewController

@property (nonatomic) NSMutableDictionary *event;

@end

NS_ASSUME_NONNULL_END
