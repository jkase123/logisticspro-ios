//
//  ProfileViewController.m
//  LX Logistics Pro
//
//  Created by Jeff Kase on 8/31/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "ProfileViewController.h"
#import "Lockbox.h"
#import "UIViewController+AppDelegate.h"

@interface ProfileViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *vibrationSwitch;

@end

@implementation ProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *sound = [Lockbox stringForKey:@"sound"];
    self.soundSwitch.on = [@"true" isEqualToString:sound];
    self.soundSwitch.onTintColor = self.appDelegate.buttonColor;
    self.soundSwitch.tintColor = self.appDelegate.buttonColor;
    NSString *vibrate = [Lockbox stringForKey:@"vibrate"];
    self.vibrationSwitch.on = [@"true" isEqualToString:vibrate];
    self.vibrationSwitch.onTintColor = self.appDelegate.buttonColor;
    self.vibrationSwitch.tintColor = self.appDelegate.buttonColor;
}

- (IBAction)eventHistoryPressed:(id)sender
{
    if (self.handler && [self.handler respondsToSelector:@selector(doEventHistory)]) {
        [self.handler doEventHistory];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)soundPressed:(UISwitch *)button
{
    [Lockbox setString:(button.on ? @"true" : @"false") forKey:@"sound"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)vibrationPressed:(UISwitch *)button
{
    [Lockbox setString:(button.on ? @"true" : @"false") forKey:@"vibrate"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)logoutPressed:(id)sender
{
    if (self.handler && [self.handler respondsToSelector:@selector(doLogout)]) {
        [self.handler doLogout];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
