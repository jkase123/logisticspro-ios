//
//  ProfileViewController.h
//  LX Logistics Pro
//
//  Created by Jeff Kase on 8/31/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@protocol ProfileHandler <NSObject>

@optional
-(void)doLogout;
-(void)doEventHistory;

@end

@interface ProfileViewController : UIViewController

@property (nonatomic) id<ProfileHandler> handler;

@end

NS_ASSUME_NONNULL_END
