//
//  HistoryDetailViewController.m
//  LX Logistics Pro
//
//  Created by Jeff Kase on 9/23/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "HistoryDetailViewController.h"
#import "NSMutableDictionary+Event.h"
#import "UIColor+Convert.h"
#import "UIViewController+AppDelegate.h"

@interface HistoryTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *text;

@end

@implementation HistoryTableCell

@end

@interface HistoryDataTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *text;

@end

@implementation HistoryDataTableCell

@end

@interface HistoryDetailViewController () <UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *assetTag;
@property (nonatomic) NSArray *tableData;
@property (nonatomic) NSMutableArray *propertyData;

@end

@implementation HistoryDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.assetTag.text = self.event.assetTag;
    self.assetTag.textColor = self.appDelegate.buttonColor;
    
    NSMutableString *location = [@"" mutableCopy];
    if (self.event.facility)
        [location appendString: self.event.facility];
    if (location.length > 0)
        [location appendString: @"\n"];
    [location appendFormat:@"%0.4f,%0.4f", self.event.latitude, self.event.longitude];
    if (self.event.city && self.event.state && self.event.postalCode) {
        [location appendFormat:@"\n%@, %@ %@", self.event.city, self.event.state, self.event.postalCode];
    }
    if (self.event.country) {
        [location appendFormat:@" %@", self.event.country];
    }
    self.tableData = @[
        @{
            @"cellIdentifier" : @"HistoryTableCell",
            @"label" : @"LOCATION",
            @"value" : location
        },
        @{
            @"cellIdentifier" : @"HistoryTableCell",
            @"label" : @"DATE/TIME",
            @"value" : self.event.timeOfLogString
        },
        @{
            @"cellIdentifier" : @"HistoryTableCell",
            @"label" : @"EVENT",
            @"value" : self.event.action
        },
        @{
            @"cellIdentifier" : @"HistoryTableCell",
            @"label" : @"UNIT",
            @"value" : (self.event.assetType ? self.event.assetType : @"")
        }
    ];

    self.propertyData = [NSMutableArray array];
    for (NSString *key in self.event.propertiesMap.allKeys) {
        if (![@"formData" isEqual:key] && ![@"formId" isEqual:key]) {
            if ([self.event.propertiesMap[key] isKindOfClass:NSString.class]) {
                [self.propertyData addObject:@{
                    @"cellIdentifier" : @"HistoryDataTableCell",
                    @"label" : key,
                    @"value" : self.event.propertiesMap[key]
                }];
            }
        }
    }

//    self.location.textColor = self.appDelegate.text2Color;
//    self.latLon.text = [NSString stringWithFormat:@"%0.4f,%0.4f", self.event.latitude, self.event.longitude];
//    self.latLon.textColor = self.appDelegate.text2Color;
//    self.note.text = self.event.note;
//    self.note.textColor = self.appDelegate.text2Color;

    self.tableView.backgroundColor = [UIColor whiteColor];
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 600;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return self.tableData.count;
    
    return self.propertyData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        HistoryTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryTableCell" forIndexPath:indexPath];
        cell.text.textColor = self.appDelegate.text2Color;
        NSDictionary *data = self.tableData[indexPath.row];
        cell.label.text = data[@"label"];
        cell.text.text = data[@"value"];
        return cell;
    }

    HistoryDataTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryDataTableCell" forIndexPath:indexPath];
    cell.text.textColor = self.appDelegate.text2Color;
    NSDictionary *data = self.propertyData[indexPath.row];
    cell.label.text = data[@"label"];
    cell.text.text = data[@"value"];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

@end
