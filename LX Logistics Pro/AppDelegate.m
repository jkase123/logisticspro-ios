//
//  AppDelegate.m
//  LX Logistics Pro
//
//  Created by Jeff Kase on 8/27/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "AppDelegate+Data.h"
#import "UIColor+Convert.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeSession];
    [self initializeLocation];
    [self initializeData];

    self.buttonColor = [UIColor colorFromHexString:@"EB4F35"];
    self.button2Color = [UIColor colorFromHexString:@"5884F5"];
    self.text1Color = [UIColor colorFromHexString:@"FFC854"];
    self.text2Color = [UIColor colorFromHexString:@"B71D1A"];
    self.text3Color = [UIColor colorFromHexString:@"A0C46D"];

    [[UINavigationBar appearance] setBarTintColor:self.text2Color];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController];

    return YES;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"***** entering foreground *****");
    if (self.token) {
        [self checkAuthToken:^(NSMutableDictionary *json) {
        }];
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
