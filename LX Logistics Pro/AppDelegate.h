//
//  AppDelegate.h
//  LX Logistics Pro
//
//  Created by Jeff Kase on 8/27/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;
@import CoreLocation;

#define kScannerClosed      @"scannerClosed"
#define kEditAsset          @"editAsset"

//#define LOCAL_URL

#ifndef LOCAL_URL
    #ifdef STAGING
        #define kAPIURLBase     @"https://api-staging.locatorx.com/lx-atlas/api/"
        #define kSettingsPageUrl @"https://lxconnect-staging.locatorx.com/settings"
        #define kCSMToken       @"ba0dad7d-532c-4282-826c-1d6449aa3ab3"
    #else
        #ifdef DEV
            #define kAPIURLBase     @"https://api-dev.locatorx.com/lx-atlas/api/"
            #define kSettingsPageUrl @"https://lxconnect-dev.locatorx.com/settings"
            #define kCSMToken       @"ba0dad7d-532c-4282-826c-1d6449aa3ab3"
        #else
            #define kAPIURLBase     @"https://api.locatorx.com/lx-atlas/api/"
            #define kSettingsPageUrl @"https://lxconnect.locatorx.com/settings"
            #define kCSMToken       @"7e38cb50-ea1a-4493-8190-5e1c59e702b6"
        #endif
    #endif
#else
    #define kAPIURLBase     @"http://10.0.0.247:8090/lx-atlas/api/"
    #define kSettingsPageUrl @"https://lxconnect-dev.locatorx.com/settings"
    #define kCSMToken       @"a90c7ec1-f45e-4807-9539-604096adf41d"
#endif

#define kAppName        @"LXConnect"

#define kPrivacyPolicy  @"https://wiki.locatorx.com/privacy-policy"
#define kTermsOfUse     @"https://wiki.locatorx.com/terms-of-use"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIColor *buttonColor, *button2Color, *text1Color, *text2Color, *text3Color;

// AppDelegate+NSURLSession
//
@property (nonatomic) NSURLSession *urlSession;
@property NSString *token, *privacyPolicy, *termsOfUse, *connectHome;
@property NSDictionary *organization, *user;
@property NSArray *forms;
@property NSDictionary *actions;

// AppDelegate+Data
//
@property (nonatomic) NSMutableDictionary *m_data;

- (NSURL *)applicationDocumentsDirectory;

// AppDelegate+Location
//
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *currentLocation;
@property bool locationServicesAuthorized, locationActive, inBackground;
@property NSTimeInterval lastLocationReportTime;
@property (copy) void(^locationBlock)(CLLocation *location);

@end
