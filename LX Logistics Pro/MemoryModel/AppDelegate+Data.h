//
//  AppDelegate+Data.h
//

@import Foundation;

#import "AppDelegate.h"

// data dictionary tags
#define kEventTag           @"event"

@interface AppDelegate (Data)

@property (nonatomic) NSMutableArray *events;

-(void)initializeData;

-(void)saveEventData;

@end
