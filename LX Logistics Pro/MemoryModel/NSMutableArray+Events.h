//
//  NSMutableArray+Events.h
//

@interface NSMutableArray (Events)

-(NSMutableDictionary *)newEvent;
-(void)addEvent:(NSMutableDictionary *)event;
-(void)deleteEvent:(NSMutableDictionary *)event;
-(NSMutableDictionary *)getEventForId:(NSString *)eventId;
-(NSMutableArray *)getEventsByTime;

@end
