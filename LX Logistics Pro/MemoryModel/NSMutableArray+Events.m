//
//  NSMutableArray+Events.m
//
//

@import Foundation;

#import "NSMutableArray+Events.h"
#import "NSCombined+Values.h"
#import "NSMutableDictionary+Event.h"

@implementation NSMutableArray (Events)

-(NSMutableDictionary *)newEvent
{
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    
    [self addObject:event];
    return event;
}

-(void)addEvent:(NSMutableDictionary *)event
{
    [self addObject:event];
}

-(void)deleteEvent:(NSMutableDictionary *)event
{
    [self removeObject:event];
}

-(NSMutableDictionary *)getEventForId:(NSString *)eventId
{
    NSArray *events = [self filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventId = %@", eventId]];
    if (events && [events count] > 0) {
        return events[0];
    }
    return nil;
}

-(NSMutableArray *)getEventsByTime
{
    return [[self sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"timeOfLog" ascending:NO]]] mutableCopy];
}

#ifdef NotNow
#define kDeviceResidentMax  6

-(void)generateEventsForList:(NSArray *)eventList
{
    NSLog(@"generateEventsForList");

    NSMutableArray *downloadedVideos = [NSMutableArray arrayWithCapacity:self.count];
    
    for (NSMutableDictionary *rev in eventList) {
        int eventId = [rev[@"eventId"] safeIntValue];
        bool newEvent = NO;
        NSMutableDictionary *event = [self getEventForId:eventId];
        if (!event) {
            event = [self newEvent];
            event.eventId = eventId;
            newEvent = YES;
            [downloadedVideos addObject:event];
        } else {
            if (event.hasLocalVideoForEvent)
                [downloadedVideos addObject:event];
        }
        [event setEventFieldsFromDict:rev newEvent:(bool)newEvent];
        
//        NSMutableDictionary *event = [self generateEventForEventId:[rev[@"eventId"] safeIntValue]];
//        [event setEventFieldsFromDict:rev newEvent:(bool)newEvent];
    }
    
    // now trim the list of resident files, ordered by time assigned
    //
    NSLog(@"downloadVideos count: %lu", (unsigned long)downloadedVideos.count);
    if (downloadedVideos.count > kDeviceResidentMax) {
        NSArray *triageList = [downloadedVideos sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timeAssigned" ascending:NO]]];
        int iCount = 0;
        for (NSMutableDictionary *event in triageList) {
            NSLog(@"checking %i: %@", iCount+1, event);
            if (iCount++ < kDeviceResidentMax) {
                [event checkLocalVideoForEventFrom:@"events"];
            } else {
                NSLog(@"  removing %@", event.localVideoFilePathForEvent);
                [[NSFileManager defaultManager] removeItemAtPath:event.localVideoFilePathForEvent error:nil];
            }
        }
    }
}

-(NSArray *)getEventsInHierarchy
{
    NSMutableDictionary *statusDict = [NSMutableDictionary dictionary];
    for (NSMutableDictionary *event in self) {
        NSNumber *stId = event[@"statusId"];
        NSMutableDictionary *stDict = statusDict[stId];
        if (!stDict) {
            statusDict[stId] = stDict = [@{@"statusId":stId,@"status":event.status,@"events":[@[] mutableCopy]} mutableCopy];
        }
        [(NSMutableArray *)stDict[@"events"] addObject:event];
    }
    NSArray *eventArray = [[statusDict allValues] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"statusId" ascending:YES]]];
    for (NSMutableDictionary *eventDict in eventArray) {
        [eventDict[@"events"] sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timeSubmitted" ascending:YES]]];
    }
    return eventArray;
}

-(NSArray *)getEventsWithSortDescs:(NSArray *)sortDescs
{
    return [self sortedArrayUsingDescriptors:sortDescs];
}

#endif

@end
