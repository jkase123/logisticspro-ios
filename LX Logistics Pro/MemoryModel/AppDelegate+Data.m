//
//  AppDelegate+Data.m
//

#import "AppDelegate+Data.h"
#import "NSCombined+Values.h"
#import "NSMutableDictionary+Event.h"

@implementation AppDelegate (Data)

-(NSMutableArray *)events
{
    return self.m_data[kEventTag];
}

-(void)setEvents:(NSMutableArray *)events
{
    self.m_data[kEventTag] = events;
}

-(void)saveEventData
{
    [self saveData:kEventTag];
}

-(NSURL *)documentUrlForTag:(NSString *)tag
{
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.json", tag]];
}

-(void)saveData:(NSString *)tag
{
    NSURL *url = [self documentUrlForTag:tag];
    NSOutputStream *outStream = [NSOutputStream outputStreamWithURL:url append:NO];
    
    [outStream open];
    
    NSInteger iCount = [NSJSONSerialization writeJSONObject:@{tag:self.m_data[tag]}
                                                   toStream:outStream
                                                    options:0
                                                      error:NULL];
    [outStream close];
    
    NSLog(@"write %li bytes", (long)iCount);
}

enum {
    kDataArray,
    kDataDictionary,
    kDataString,
    kDataNumber
};

-(void)loadData:(NSString *)tag type:(int)dataType
{
    NSURL *url = [self documentUrlForTag:tag];
//    NSLog(@"url %@", url.absoluteString);
    NSInputStream *inStream = [NSInputStream inputStreamWithURL:url];
    if (inStream) {
        
        [inStream open];
        NSDictionary *data = [NSJSONSerialization JSONObjectWithStream:inStream
                                                               options:NSJSONReadingMutableContainers error:nil];
        [inStream close];
        
        if (data && data[tag]) {
            self.m_data[tag] = data[tag];
            return;
        }
    }
    switch (dataType) {
        case kDataArray:
            self.m_data[tag] = [NSMutableArray array];
            break;
        case kDataDictionary:
            self.m_data[tag] = [NSMutableDictionary dictionary];
            break;
        case kDataString:
            self.m_data[tag] = [NSString string];
            break;
        case kDataNumber:
            self.m_data[tag] = @0;
            break;
    }
}

-(void)initializeData
{
    self.m_data = [NSMutableDictionary dictionaryWithCapacity:3];
    
    [self loadData:kEventTag type:kDataArray];
    
//    NSDateFormatter *dfRaw = [[NSDateFormatter alloc] init];
//    [dfRaw setDateFormat:@"yyyyMMddHHmmss"];

    NSDateFormatter *dfOut = [[NSDateFormatter alloc] init];
    [dfOut setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMddyyyyhhmmssa" options:0 locale:NSLocale.systemLocale]];

    NSInteger timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];

    for (NSMutableDictionary *event in self.events) {
        NSDate *gmtDate = [NSDate dateWithTimeIntervalSince1970:event.timeOfLog];
        NSDate *localDate = [gmtDate dateByAddingTimeInterval:timeZoneSeconds];
        event.timeOfLogString = [dfOut stringFromDate:localDate];
    }
    [self saveEventData];
}

@end
