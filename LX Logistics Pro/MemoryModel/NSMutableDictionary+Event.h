//
//  NSMutableDictionary+Event.h
//

@import Foundation;
@import UIKit;

@interface NSMutableDictionary (Event)

@property (nonatomic) NSString *assetId, *assetTag, *assetType, *batchId, *productId, *action, *timeOfLogString, *city, *state, *country, *postalCode, *facility, *location, *note;
@property (nonatomic) NSMutableDictionary *propertiesMap;
@property int timeOfLog;
@property float latitude, longitude;

@end
