//
//  NSMutableDictionary+Event.m
//

@import Foundation;

#import "NSMutableDictionary+Event.h"
#import "NSCombined+Values.h"

@implementation NSMutableDictionary (Event)

-(NSString *)assetId
{
    return [self[@"assetId"] safeStringValue];
}

-(void)setAssetId:(NSString *)assetId
{
    self[@"assetId"] = assetId;
}

-(NSString *)batchId
{
    return [self[@"batchId"] safeStringValue];
}

-(void)setBatchId:(NSString *)batchId
{
    self[@"batchId"] = batchId;
}

-(NSString *)productId
{
    return [self[@"productId"] safeStringValue];
}

-(void)setProductId:(NSString *)productId
{
    self[@"productId"] = productId;
}

-(NSString *)action
{
    return [self[@"action"] safeStringValue];
}

-(void)setAction:(NSString *)action
{
    self[@"action"] = action;
}

-(NSString *)assetTag
{
    return [self[@"assetTag"] safeStringValue];
}

-(void)setAssetTag:(NSString *)assetTag
{
    self[@"assetTag"] = assetTag;
}

-(NSString *)assetType
{
    return [self[@"assetType"] safeStringValue];
}

-(void)setAssetType:(NSString *)assetType
{
    self[@"assetType"] = assetType;
}

-(NSString *)facility
{
    return [self[@"facility"] safeStringValue];
}

-(void)setFacility:(NSString *)facility
{
    self[@"facility"] = facility;
}

-(NSString *)note
{
    return [self[@"note"] safeStringValue];
}

-(void)setNote:(NSString *)note
{
    self[@"note"] = note;
}

-(NSString *)location
{
    return [self[@"location"] safeStringValue];
}

-(void)setLocation:(NSString *)location
{
    self[@"location"] = location;
}

-(NSString *)city
{
    return [self[@"city"] safeStringValue];
}

-(void)setCity:(NSString *)city
{
    self[@"city"] = city;
}

-(NSString *)state
{
    return [self[@"state"] safeStringValue];
}

-(void)setState:(NSString *)state
{
    self[@"state"] = state;
}

-(NSString *)country
{
    return [self[@"country"] safeStringValue];
}

-(void)setCountry:(NSString *)country
{
    self[@"country"] = country;
}

-(NSString *)postalCode
{
    return [self[@"postalCode"] safeStringValue];
}

-(void)setPostalCode:(NSString *)postalCode
{
    self[@"postalCode"] = postalCode;
}

-(NSString *)timeOfLogString
{
    return [self[@"timeOfLogString"] safeStringValue];
}

-(void)setTimeOfLogString:(NSString *)timeOfLogString
{
    self[@"timeOfLogString"] = timeOfLogString;
}

-(NSMutableDictionary *)propertiesMap
{
    return self[@"propertiesMap"];
}

-(void)setPropertiesMap:(NSMutableDictionary *)propertiesMap
{
    self[@"propertiesMap"] = propertiesMap;
}

-(int)timeOfLog
{
    return [self[@"timeOfLog"] safeIntValue];
}

-(void)setTimeOfLog:(int)timeOfLog
{
    self[@"timeOfLog"] = [NSNumber numberWithInt:timeOfLog];
}

-(float)latitude
{
    return [self[@"latitude"] safeFloatValue];
}

-(void)setLatitude:(float)latitude
{
    self[@"latitude"] = [NSNumber numberWithFloat:latitude];
}

-(float)longitude
{
    return [self[@"longitude"] safeFloatValue];
}

-(void)setLongitude:(float)longitude
{
    self[@"longitude"] = [NSNumber numberWithFloat:longitude];
}

@end
