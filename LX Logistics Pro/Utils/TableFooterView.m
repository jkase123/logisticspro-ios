//
//  TableFooterView.m
//  LX Connect
//
//  Created by Jeff Kase on 4/12/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "TableFooterView.h"

@interface TableFooterView ()

@property (nonatomic) NSString *message;
@property bool hasData;

@end

@implementation TableFooterView

-(id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.opaque = NO;
        self.hasData = YES;
        self.backgroundColor = [UIColor clearColor];
        
        self.message = @"No files available";
    }
    return self;
}

-(void)setHasData:(bool)hasData alternateMessage:(NSString *)message
{
    self.hasData = hasData;
    
    self.message = message;
    
    [self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect
{
    if (!self.hasData) {
        UIFont *theFont = [UIFont boldSystemFontOfSize:16];
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSaveGState(context);
        
        [[UIColor blackColor] set];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.alignment = NSTextAlignmentCenter;
        [self.message drawInRect:CGRectMake(0, (rect.size.height-20)/2, rect.size.width, 20) withAttributes:@{NSFontAttributeName:theFont, NSParagraphStyleAttributeName:style}];
        
        CGContextRestoreGState(context);
    }
}

@end
